## 注意事項
+ 此專案已修改為只可以在EditorMode讀取，但還是有保留用Http抓取素材的Code
+ 預設企劃人員會寫json，讀取資料型態皆為json
+ 回合制移動使用Dotween
+ Camera移動方式(wasd)
 
## 使用技巧

+ Factory
+ Singleton
+ Interface
+ ObjectPool
+ AssetBundle
+ Resource
+ 手動讀取Binary檔(讀取Npc資料不使用JsonUtility)
+ Astar
+ Jenkins
+ Http

## Scene
+ NPCBattleSystem(戰鬥)
+ PlantNPCSaveSystemScene(種植Npc)
