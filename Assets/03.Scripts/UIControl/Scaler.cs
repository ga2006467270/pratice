﻿using UnityEngine;


class Scaler : MonoBehaviour
{
    const float ORIGIANLE_SIZE = 0.75f;

    [SerializeField] GameObject mScaleImage;

    void Start()
    {
        var picturewidth = Screen.width;
        var pictureheight = Screen.height;
        if (picturewidth > pictureheight)
        {
            mScaleImage.transform.localScale = new Vector3(mScaleImage.transform.localScale.x * picturewidth / pictureheight * ORIGIANLE_SIZE, mScaleImage.transform.localScale.y, mScaleImage.transform.localScale.z);
        }
        else
        {
            mScaleImage.transform.localScale = new Vector3(mScaleImage.transform.localScale.x, mScaleImage.transform.localScale.y * pictureheight / picturewidth * 1 / ORIGIANLE_SIZE, mScaleImage.transform.localScale.z);
        }
    }
}
