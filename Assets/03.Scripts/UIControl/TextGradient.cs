﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[AddComponentMenu("UI/Effects/Gradient")]
public class TextGradient : BaseMeshEffect
{
    const float GRADIENT_START_POS = -1f;

    [SerializeField] Color32 TopColor = Color.white;
    [SerializeField] Color32 BottomColor = Color.black;

    public override void ModifyMesh(VertexHelper vertexhelper)
    {
        var buttomy = GRADIENT_START_POS;
        var topy = GRADIENT_START_POS;

        for (int vertexcount = 0; vertexcount < vertexhelper.currentVertCount; vertexcount++)
        {
            var uivertex = new UIVertex();
            vertexhelper.PopulateUIVertex(ref uivertex, vertexcount);

            if (buttomy == GRADIENT_START_POS)
                buttomy = uivertex.position.y;
            if (topy == GRADIENT_START_POS)
                topy = uivertex.position.y;

            if (uivertex.position.y > topy)
                topy = uivertex.position.y;
            else if (uivertex.position.y < buttomy)
                buttomy = uivertex.position.y;
        }

        float uielementheight = topy - buttomy;

        for (int vertexcount = 0; vertexcount < (vertexhelper.currentVertCount); vertexcount++)
        {
            var uivertex = new UIVertex();
            vertexhelper.PopulateUIVertex(ref uivertex, vertexcount);

            uivertex.color = Color32.Lerp(BottomColor, TopColor, (uivertex.position.y - buttomy) * uielementheight);
            vertexhelper.SetUIVertex(uivertex, vertexcount);
        }
    }
}
