﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonClicker : MonoBehaviour
{
    const float CLICK_ANIMCOUNT = 0.1f;
    const float CLICK_SIZE = 0.05f;

    [SerializeField] Sprite mCurrentImage;
    [SerializeField] GameObject mCurrentButton;
    [SerializeField] Image mCurrentButtonImange;
    [SerializeField] List<Image> mMenuButtonList = new List<Image>();
    [SerializeField] List<GameObject> mTargetPanel = new List<GameObject>();
    List<Transform> mClickList = new List<Transform>();
    Dictionary<GameObject, GameObject> mButtonPanelDic = new Dictionary<GameObject, GameObject>();

    public void ButtonOnClick(Transform buttontrans)
    {
        if (mClickList.Contains(buttontrans))
        {
            return;
        }
        mClickList.Add(buttontrans);
        StartCoroutine(ClickCoroutine(buttontrans));
    }

    public void ImageChange(Image targetGameObject)
    {
        if (targetGameObject == mCurrentButton)
        {
            return;
        }
        mCurrentButtonImange.sprite = targetGameObject.sprite;
        targetGameObject.sprite = mCurrentImage;
        mCurrentButtonImange = targetGameObject;
        mButtonPanelDic[mCurrentButton].SetActive(false);
        mButtonPanelDic[targetGameObject.gameObject].SetActive(true);
        mCurrentButton = targetGameObject.gameObject;
    }

    void Start()
    {
        MenuButtonSet();
    }

    void MenuButtonSet()
    {
        var listcount = 0;
        foreach (GameObject targetpanel in mTargetPanel)
        {
            mButtonPanelDic.Add(mMenuButtonList[listcount].gameObject, targetpanel.gameObject);
            if (listcount != 0)
            {
                targetpanel.gameObject.SetActive(false);
            }
            listcount++;
        }
        mCurrentButton = mMenuButtonList[0].gameObject;
        mCurrentButtonImange = mMenuButtonList[0];
    }

    IEnumerator ClickCoroutine(Transform buttontrans)
    {
        var oriainaltransform = buttontrans.localScale;
        var timecounter = 0f;
        while (timecounter < CLICK_ANIMCOUNT)
        {
            buttontrans.localScale = new Vector3(buttontrans.localScale.x + CLICK_SIZE * ((buttontrans.localScale.x) > 0 ? 1 : -1), buttontrans.localScale.y + CLICK_SIZE, buttontrans.localScale.z);
            timecounter += Time.deltaTime;
            yield return new WaitForFixedUpdate();
        }
        timecounter = 0f;
        while (timecounter < CLICK_ANIMCOUNT)
        {
            buttontrans.localScale = new Vector3(buttontrans.localScale.x - CLICK_SIZE * ((buttontrans.localScale.x) > 0 ? 1 : -1), buttontrans.localScale.y - CLICK_SIZE, buttontrans.localScale.z);
            timecounter += Time.deltaTime;
            yield return new WaitForFixedUpdate();
        }
        buttontrans.localScale = oriainaltransform;
        mClickList.Remove(buttontrans);
    }
}
