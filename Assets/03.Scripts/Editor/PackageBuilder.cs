﻿using UnityEditor;
using System;
using System.IO;
using UnityEngine;

#region Build apk By Code
public static class PackageBuilder
{
    static string mDataPath = Application.dataPath.Remove(Application.dataPath.Length - AssetBundleManager.ASSET_LENGTH);

    /// <summary>
    /// 製作apk
    /// </summary>
    [MenuItem("Auto Builder/Build Window")]
    public static void PerformBuild()
    {
        string path = null;
        string[] defaultscene = { "assets/02.Scenes/NPCBattleSystem.unity" };
        if (UnityEditorInternal.InternalEditorUtility.inBatchMode)
        {
            path = GetBatchModeBuildPath();
        }
        if (!File.Exists(mDataPath + "/builds"))
        {
            Directory.CreateDirectory(mDataPath + "/builds");
        }
        string[] files = Directory.GetFiles("./builds");
        foreach (string file in files)
        {
            File.SetAttributes(file, FileAttributes.Normal);
            File.Delete(file);
        }
        if (path != null)
        {
            BuildPipeline.BuildPlayer(defaultscene, "./builds/BattleSystemScene" + path + DateTime.Now.Month.ToString() + "_" + DateTime.Now.Day.ToString() + "_" + DateTime.Now.Hour.ToString() + "_" + DateTime.Now.Minute.ToString() + "_" + ".exe", BuildTarget.StandaloneWindows, BuildOptions.None);
        }
        else
        {
            BuildPipeline.BuildPlayer(defaultscene, "./builds/BattleSystemScene_null" + "_" + DateTime.Now.Month.ToString() + "_" + DateTime.Now.Day.ToString() + "_" + DateTime.Now.Hour.ToString() + "_" + DateTime.Now.Minute.ToString() + "_" + ".exe", BuildTarget.StandaloneWindows, BuildOptions.None);
        }
    }
    /// <summary>
    /// 製作AssetBundle
    /// </summary>
    [MenuItem("Assets/Build AssetBundles(Windows)")]
    public static void BuildAssetBundle()
    {
        string path = GetBatchModeBuildPath();
        if (!File.Exists(mDataPath + "/builds"))
        {
            Directory.CreateDirectory(mDataPath + "/builds");
        }
        if (path != null)
        {
            if (!File.Exists(mDataPath + "./bundles" + path))
            {
                Directory.CreateDirectory(mDataPath + "./bundles" + "/" + path);
            }
            switch (path)
            {
                case "Android":
                    BuildPipeline.BuildAssetBundles("./bundles" + "/" + path, BuildAssetBundleOptions.ChunkBasedCompression, BuildTarget.Android);
                    break;
                case "Editor":
                    BuildPipeline.BuildAssetBundles("./bundles" + "/" + path, BuildAssetBundleOptions.ChunkBasedCompression, BuildTarget.StandaloneWindows);
                    break;
                default:
                    Debug.Log("錯誤的path");
                    break;
            }
        }
        else
        {
            BuildPipeline.BuildAssetBundles("./bundles", BuildAssetBundleOptions.ChunkBasedCompression, BuildTarget.Android);
        }
    }

    [MenuItem("Auto Builder/Build JsonFile(Normal)")]
    public static void BuildJsonData()
    {
        string path = GetBatchModeBuildPath();
        if (path != null)
        {
            switch (path)
            {
                case "Normal":
                    AssetBundleDataListManager.SaveJsonFile(mDataPath + "./bundles", "AssetBundles", AssetBundleManager.eVersion.Normal, AssetLoadURL.GetAssetLoadSystemURL(AssetLoadURL.eBundleSaveVersion.AssetBundleSystem));
                    break;
                case "Chrismas":
                    AssetBundleDataListManager.SaveJsonFile(mDataPath + "./bundles", "AssetBundles", AssetBundleManager.eVersion.Chrismas, AssetLoadURL.GetAssetLoadSystemURL(AssetLoadURL.eBundleSaveVersion.AssetBundleSystem));
                    break;
                default:
                    Debug.Log("Eorror");
                    break;
            }
        }
        else
        {
            AssetBundleDataListManager.SaveJsonFile(mDataPath + "./bundles", "AssetBundles", AssetBundleManager.eVersion.Normal, AssetLoadURL.GetAssetLoadSystemURL(AssetLoadURL.eBundleSaveVersion.AssetBundleSystem));
        }
    }

    static string GetBatchModeBuildPath()
    {
        var args = Environment.GetCommandLineArgs();
        for (int textlength = 0; textlength < args.Length; ++textlength)
        {
            if (args[textlength].ToLower() == "-output")
            {
                if (textlength + 1 < args.Length)
                {
                    return args[textlength + 1];
                }
            }
        }
        return null;
    }
}
#endregion
