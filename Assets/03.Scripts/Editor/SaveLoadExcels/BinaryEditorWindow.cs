﻿using UnityEngine;
using UnityEditor;

public class BinaryEditorWindow : EditorWindow
{
    const int ZERO_LENGTH = 0;

    static string mSavePath = "無";
    static string mExcelName = "";

    bool mIsChooseThePath = false;
    bool mIsChooseTheName = false;

    [MenuItem("EditorWindow/BinaryEditorWindow")]
    public static void ShowWindow()
    {
        GetWindow<BinaryEditorWindow>("BinaryEditor");
    }

    void OnGUI()
    {
        LoadFile();
        TurnBinary();
        LoadBinary();
        ChooseStructType();
    }

    /// <summary>
    /// 讀取檔案的GUI
    /// </summary>
    void LoadFile()
    {
        GUIStyle text = new GUIStyle(EditorStyles.whiteLargeLabel);
        text.normal.textColor = Color.blue;
        GUILayout.Label("檔案選擇", text);
        if (GUILayout.Button("Excel檔案讀取"))
        {
            LoadFilePath();
        }
    }

    /// <summary>
    /// 轉換Excel檔案為Library的GUI
    /// </summary>
    void TurnBinary()
    {
        GUIStyle text = new GUIStyle(EditorStyles.whiteLargeLabel);
        text.normal.textColor = Color.blue;
        GUILayout.Label("轉換檔案為Binary", text);
        EditorGUILayout.BeginHorizontal();

        if (GUILayout.Button("選擇Binary儲存路徑"))
        {
            ChooseSaveFilePath();
            if (mSavePath != "無")
            {
                mIsChooseTheName = true;
            }
            else
            {
                mIsChooseTheName = false;
            }
        }

        GUILayout.Label("目前儲存路徑 : " + mSavePath);
        EditorGUILayout.EndHorizontal();
        if (mIsChooseTheName)
        {
            EditorGUILayout.BeginHorizontal();
            GUILayout.Label("Binary名稱");
            mExcelName = GUILayout.TextField(mExcelName);
            if (mExcelName != "")
            {
                mIsChooseThePath = true;
            }
            else
            {
                mIsChooseThePath = false;
            }
            EditorGUILayout.EndHorizontal();
        }
        if (mIsChooseThePath && mSavePath != "無")
        {
            if (GUILayout.Button("開始轉換Binary"))
            {
                if (mSavePath == "無")
                {
                    Debug.LogError("請指定一個儲存路徑");
                    return;
                }
                TransformExcelToBinary();
            }
        }
        else if (mSavePath == "無")
        {
            GUIStyle notsettext = new GUIStyle(EditorStyles.toolbarButton);
            notsettext.normal.textColor = Color.red;
            GUILayout.Button("尚未設定路徑", notsettext);
        }
        else
        {
            GUIStyle notsettext = new GUIStyle(EditorStyles.toolbarButton);
            notsettext.normal.textColor = Color.red;
            GUILayout.Button("尚未決定Binary名稱", notsettext);
        }
    }

    /// <summary>
    /// 讀取binaray的GUI
    /// </summary>
    void LoadBinary()
    {
        GUIStyle text = new GUIStyle(EditorStyles.whiteLargeLabel);
        text.normal.textColor = Color.blue;
        GUILayout.Label("讀取Binary檔案", text);
        if (GUILayout.Button("讀取Binary"))
        {
            LoadExcelBinaryData();
        }
    }

    /// <summary>
    /// 選擇資料型態的GUI
    /// </summary>
    void ChooseStructType()
    {
        GUIStyle text = new GUIStyle(EditorStyles.whiteLargeLabel);
        text.normal.textColor = Color.blue;
        GUILayout.Label("選擇資料型態", text);
        string[] options = { "物品", "武將" };
        GUILayout.BeginHorizontal();
        GUILayout.Label("資料型態(StructType)");
        SaveLoadStructure.Index = EditorGUILayout.Popup(SaveLoadStructure.Index, options);
        GUILayout.EndHorizontal();
    }

    /// <summary>
    /// 讀取出File位置
    /// </summary>
    void LoadFilePath()
    {
        string path = EditorUtility.OpenFilePanel("", "", "xlsx");
        if (path.Length != ZERO_LENGTH)
        {
            Excel xls = ExcelHelper.LoadExcel(path);
            string[,,] logArray = xls.GetLogArray();
            SaveLoadStructure.IndexSearcher(SaveLoadStructure.Index, logArray);
            Debug.Log("讀取完畢");
        }
    }

    /// <summary>
    /// 轉換Excel檔為Binary格式
    /// </summary>
    void TransformExcelToBinary()
    {
        string path = EditorUtility.OpenFilePanel("", "", "xlsx");
        if (path.Length != ZERO_LENGTH)
        {
            SaveLoad saveload = new SaveLoad(SaveLoad.eSystemType.Excel);
            Excel xls = ExcelHelper.LoadExcel(path);
            string[,,] logArray = xls.GetLogArray();
            SaveLoadStructure.LogArray = logArray;
            saveload.Save(mSavePath, mExcelName);
            Debug.Log("轉換完成");
        }
    }

    /// <summary>
    /// 讀取Binary檔案
    /// </summary>
    void LoadExcelBinaryData()
    {
        string path = EditorUtility.OpenFilePanel("", "", "txt");
        SaveLoad saveload = new SaveLoad(SaveLoad.eSystemType.Excel);
        saveload.Load(path);
    }

    /// <summary>
    /// 存檔路徑
    /// </summary>
    void ChooseSaveFilePath()
    {
        string path = EditorUtility.OpenFolderPanel("", "", "");
        if (path.Length != ZERO_LENGTH)
        {
            mSavePath = path;
        }
    }
}
