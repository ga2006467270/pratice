﻿using UnityEngine;

public interface IAssetLoadType
{
    void LoadAssetBundle();
}

public class AssetLoadURL
{
    public struct sBundleStruct
    {
        public string BundleName;
        public ObjectPoolManager.eObjectPoolType PoolType;
    }

    public enum eBundleSaveVersion
    {
        AssetBundleSystem,
    }

    static sBundleStruct[] mAssetBundleSystemURL;

    public static sBundleStruct[] GetAssetLoadSystemURL(eBundleSaveVersion version)
    {
        sBundleStruct[] assetbundlesystemurl;
        switch (version)
        {
            case eBundleSaveVersion.AssetBundleSystem:
                assetbundlesystemurl = new sBundleStruct[BattleManager.MAX_CHARACTER];
                return AssetBundleSystemStruct(assetbundlesystemurl);
            default:
                Debug.LogError("沒有這個version");
                break;
        }
        return null;
    }

    static sBundleStruct[] AssetBundleSystemStruct(sBundleStruct[] assetBundleSystemURL)
    {
        assetBundleSystemURL[0].PoolType = ObjectPoolManager.eObjectPoolType.ChracterPool;
        assetBundleSystemURL[0].BundleName = "characterbundle";
        assetBundleSystemURL[1].PoolType = ObjectPoolManager.eObjectPoolType.ObjectPool;
        assetBundleSystemURL[1].BundleName = "uicundle";
        assetBundleSystemURL[2].PoolType = ObjectPoolManager.eObjectPoolType.ObjectPool;
        assetBundleSystemURL[2].BundleName = "enviroment";
        assetBundleSystemURL[3].PoolType = ObjectPoolManager.eObjectPoolType.ObjectPool;
        assetBundleSystemURL[3].BundleName = "other";
        assetBundleSystemURL[4].PoolType = ObjectPoolManager.eObjectPoolType.TexturePool;
        assetBundleSystemURL[4].BundleName = "texture";
        assetBundleSystemURL[5].PoolType = ObjectPoolManager.eObjectPoolType.MaterialPool;
        assetBundleSystemURL[5].BundleName = "material";
        return assetBundleSystemURL;
    }

}

/// <summary>
/// 獲取網路上的json檔來選擇assetBundle要讀取哪個檔案
/// </summary>
public class AssetLoad : IAssetLoadType
{
    public void LoadAssetBundle()
    {
        // 修改為不需要http模式
        SaveLoad load = new SaveLoad(SaveLoad.eSystemType.AssetBundle);
        //string path = URLManager.GetURL(AssetBundleManager.Instance.PlayModePlatform, AssetBundleManager.ASSET_FILE_NAME, AssetBundleManager.Instance.Version, SaveFileFormat.SaveFromat(eSaveFileFormat.json));
        string path = URLManager.GetProjectFolderPath() + "bundles";
        load.Load(path);
    }
}
