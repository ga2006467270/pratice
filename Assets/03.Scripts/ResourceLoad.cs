﻿using UnityEngine;

/// <summary>
/// Resource工廠
/// </summary>
public class ResourceLoad : MonoBehaviour
{
    public enum eSystemType
    {
        PlantNPCSave,
        Move,
        NPCInteract,
        NPCExcelPlant,
        LoadAssetBundle,
    }

    public static ResourceLoad Instance
    {
        get
        {
            if (mInstance == null)
            {
                var gamemanager = new GameObject("ResourceLoad");
                gamemanager.AddComponent<ResourceLoad>();
            }
            return mInstance;
        }
    }

    public bool AddInteractable;

    [SerializeField] eSystemType mSystemType;
    static ResourceLoad mInstance;
    RsourceStores mFactoryStore;

    public eSystemType GetSystemType()
    {
        return mSystemType;
    }

    void Awake()
    {
        mInstance = this;
        ObjectPoolManager.Instance.SetSerializeObjectPool();
        mFactoryStore = new RsourceStores(new SimpleResourceFactory());
        mFactoryStore.ResourceOrder(mSystemType);
        ObjectPoolManager.Instance.LoadObjectPool();
    }

}

public class RsourceStores
{
    SimpleResourceFactory mFactory;

    public RsourceStores(SimpleResourceFactory factory)
    {
        mFactory = factory;
    }

    public void ResourceOrder(ResourceLoad.eSystemType resourceEnum)
    {
        IResourseLoadType resourcetype;
        resourcetype = mFactory.CreateResource(resourceEnum);
        resourcetype.LoadResourses();
    }
}

public class SimpleResourceFactory
{
    public IResourseLoadType CreateResource(ResourceLoad.eSystemType resourceEnum)
    {
        switch (resourceEnum)
        {
            case ResourceLoad.eSystemType.Move:
                return new ResourceLoadMove();
            case ResourceLoad.eSystemType.PlantNPCSave:
                return new ResourceLoadPlantNPCSave();
            case ResourceLoad.eSystemType.NPCInteract:
                return new ResourceLoadNPCInteract();
            case ResourceLoad.eSystemType.NPCExcelPlant:
                return new ResourceLoadNPCInteractPlant();
            case ResourceLoad.eSystemType.LoadAssetBundle:
                return new ResourceLoadAssetBundle();
        }
        return null;
    }
}
