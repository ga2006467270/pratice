﻿using UnityEngine;

public class GridDetectHelper : MonoBehaviour
{
    const float DETECT_POINT_OFFSET = 0.1f;

    public static GridDetectHelper Instance
    {
        get
        {
            if (mInstance == null)
            {
                var gamemanager = new GameObject("GridDetectHelper");
                gamemanager.AddComponent<GridDetectHelper>();
            }
            return mInstance;
        }
    }

    public Material DetectGridMat
    {
        get
        {
            if (mDetectGridMat == null)
            {
                mDetectGridMat = ObjectPoolManager.Instance.GetMaterialFromMaterialPool("DetectGrid");
                if (mDetectGridMat == null)
                {
                    mDetectGridMat = Resources.Load<Material>("PlantNPCSaveSystem/Material/DetectGrid");
                }
            }
            return mDetectGridMat;
        }
        set
        {
            mDetectGridMat = value;
        }
    }

    static GridDetectHelper mInstance;
    Material mDetectGridMat;
    GameObject mDetectGrid;

    public void GridDected(Vector3 detectedPoint)
    {
        Node node = Grids.Instance.NodeFromWorldPoint(detectedPoint);
        mDetectGrid.transform.position = node.Pos;
        mDetectGrid.transform.position = new Vector3(mDetectGrid.transform.position.x, detectedPoint.y + DETECT_POINT_OFFSET, mDetectGrid.transform.position.z);
        if (!node.Walkable || node.HasGameObject != null)
        {
            DetectGridMat.color = Color.yellow;
        }
        else
        {
            DetectGridMat.color = Color.white;
        }
    }

    void Awake()
    {
        mInstance = this;
    }

    void Start()
    {
        mDetectGrid = ObjectPoolManager.Instance.GetObjectInGame("DectectGrid");
        mDetectGrid.transform.localScale *= Grids.Instance.NodeRaius;
    }
}
