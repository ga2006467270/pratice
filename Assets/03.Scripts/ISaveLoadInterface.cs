﻿using UnityEngine;

public interface ISaveLoadInterface
{
    void Save(string path, string name);
    void Load(string path);
}

public class SaveFileFormat
{
    public static string SaveFromat(eSaveFileFormat saveFormat)
    {
        switch (saveFormat)
        {
            case eSaveFileFormat.json:
                return ".json";
            case eSaveFileFormat.txt:
                return ".txt";
            default:
                Debug.LogError("沒有這種存檔格式");
                return null;
        }
    }
}

public enum eSaveFileFormat
{
    json,
    txt,
}