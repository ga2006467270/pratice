﻿using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class NPCBattleInteractUI : NPCPlantInteractUI
{
    [SerializeField] InputField mCharacterBattleNumberOneInputText;
    [SerializeField] InputField mCharacterBattleNumberTwoInputText;
    [SerializeField] InputField mCharacterBattleNumberThreeInputText;
    [SerializeField] InputField mCharacterBattleNumberFourInputText;
    [SerializeField] InputField mCharacterBattleNumberFiveInputText;
    [SerializeField] InputField mCharacterBattleNumberSixInputText;

    public void LoadInteractBattleBinary()
    {
        SaveLoad loadmonsterdata = new SaveLoad(SaveLoad.eSystemType.Battle);
        loadmonsterdata.Load(URLManager.GetURL(AssetBundleManager.ePlatform.Editor, "Monsterdb", AssetBundleManager.eVersion.Normal, SaveFileFormat.SaveFromat(eSaveFileFormat.json)));
    }

    public void CharacterInteractDropDownIndexChange(int index)
    {
        if (PlantManager.Instance.EditorMode == PlantManager.eEditorMode.Edit)
        {
            PlantManager.Instance.CurrentNode.CanTalk = (index == 1) ? true : false;
            Debug.Log("更換對話為 : " + PlantManager.Instance.CurrentNode.CanTalk);
        }
        else
        {
            Debug.LogError("請在修改模式下使用此功能");
        }
    }

    public void ChangeMonsterOneSpeices()
    {
        var inputtext = mCharacterBattleNumberOneInputText.text;
        mCharacterBattleNumberOneInputText.text = "";
        int battlenumber;
        var isint = int.TryParse(inputtext, out battlenumber);
        if (PlantManager.Instance.EditorMode == PlantManager.eEditorMode.Edit)
        {
            if (!isint)
            {
                Debug.LogError("請輸入數字");
                return;
            }
            PlantManager.Instance.CurrentNode.CharacterBattleNumber[0] = int.Parse(inputtext);
        }
        else
        {
            Debug.LogError("請在修改模式下使用此功能");
        }
    }

    public void ChangeMonsterTwoSpeices()
    {
        var inputtext = mCharacterBattleNumberTwoInputText.text;
        mCharacterBattleNumberTwoInputText.text = "";
        int battlenumber;
        var isint = int.TryParse(inputtext, out battlenumber);
        if (PlantManager.Instance.EditorMode == PlantManager.eEditorMode.Edit)
        {
            if (!isint)
            {
                Debug.LogError("請輸入數字");
                return;
            }
            PlantManager.Instance.CurrentNode.CharacterBattleNumber[1] = int.Parse(inputtext);
        }
        else
        {
            Debug.LogError("請在修改模式下使用此功能");
        }
    }

    public void ChangeMonsterThreeSpeices()
    {
        var inputtext = mCharacterBattleNumberThreeInputText.text;
        mCharacterBattleNumberThreeInputText.text = "";
        int battlenumber;
        var isint = int.TryParse(inputtext, out battlenumber);
        if (PlantManager.Instance.EditorMode == PlantManager.eEditorMode.Edit)
        {
            if (!isint)
            {
                Debug.LogError("請輸入數字");
                return;
            }
            PlantManager.Instance.CurrentNode.CharacterBattleNumber[2] = int.Parse(inputtext);
        }
        else
        {
            Debug.LogError("請在修改模式下使用此功能");
        }
    }

    public void ChangeMonsterFourSpeices()
    {
        var inputtext = mCharacterBattleNumberFourInputText.text;
        mCharacterBattleNumberFourInputText.text = "";
        int battlenumber;
        var isint = int.TryParse(inputtext, out battlenumber);
        if (PlantManager.Instance.EditorMode == PlantManager.eEditorMode.Edit)
        {
            if (!isint)
            {
                Debug.LogError("請輸入數字");
                return;
            }
            PlantManager.Instance.CurrentNode.CharacterBattleNumber[3] = int.Parse(inputtext);
        }
        else
        {
            Debug.LogError("請在修改模式下使用此功能");
        }
    }

    public void ChangeMonsterFiveSpeices()
    {
        var inputtext = mCharacterBattleNumberFiveInputText.text;
        mCharacterBattleNumberFiveInputText.text = "";
        int battlenumber;
        var isint = int.TryParse(inputtext, out battlenumber);
        if (PlantManager.Instance.EditorMode == PlantManager.eEditorMode.Edit)
        {
            if (!isint)
            {
                Debug.LogError("請輸入數字");
                return;
            }
            PlantManager.Instance.CurrentNode.CharacterBattleNumber[4] = int.Parse(inputtext);
        }
        else
        {
            Debug.LogError("請在修改模式下使用此功能");
        }
    }

    public void ChangeMonsterSixSpeices()
    {
        var inputtext = mCharacterBattleNumberSixInputText.text;
        mCharacterBattleNumberSixInputText.text = "";
        int battlenumber;
        var isint = int.TryParse(inputtext, out battlenumber);
        if (PlantManager.Instance.EditorMode == PlantManager.eEditorMode.Edit)
        {
            if (!isint)
            {
                Debug.LogError("請輸入數字");
                return;
            }
            PlantManager.Instance.CurrentNode.CharacterBattleNumber[5] = int.Parse(inputtext);
        }
        else
        {
            Debug.LogError("請在修改模式下使用此功能");
        }
    }
}
