﻿using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class NPCPlantInteractUI : NPCSaveUIManager
{
    const int ZERO_LENGTH = 0;
    const int INDEX_STURCTURE = 2;

    [SerializeField] protected InputField mSerialNumberInputText;
    [SerializeField] protected InputField mContentInputText;
    [SerializeField] protected Text mCurrentSexText;
    [SerializeField] protected Text mCurrentBattleText;
    [SerializeField] protected Text mCurrentSerialText;

    /// <summary>
    /// 讀取Excel檔案
    /// </summary>
    public void LoadExcel()
    {
#if UNITY_EDITOR
        string path = EditorUtility.OpenFilePanel("Panel", "none", "xlsx");
        if (path.Length != ZERO_LENGTH)
        {
            Excel xls = ExcelHelper.LoadExcel(path);
            string[,,] logArray = xls.GetLogArray();
            SaveLoadStructure.IndexSearcher(INDEX_STURCTURE, logArray);
            Debug.Log("讀取完畢");
        }
#endif
    }

    /// <summary>
    /// 轉換為Binary檔案
    /// </summary>
    public void TurnToBinary()
    {
#if UNITY_EDITOR
        mPath = EditorUtility.OpenFolderPanel("", "", "");
        SaveLoad save = new SaveLoad(SaveLoad.eSystemType.NPCInterct);
        save.Save(mPath, "/NPCdb");
#endif
    }

    /// <summary>
    /// 清單轉換角色性別
    /// </summary>
    /// <param name="index">性別</param>
    public void CharacterSexDropDownIndexChange(int index)
    {
        if (PlantManager.Instance.EditorMode == PlantManager.eEditorMode.Edit)
        {
            PlantManager.Instance.CurrentNode.Sex = index;
            ChangeSexValue();
            Debug.Log("更換性別");
        }
        else
        {
            Debug.LogError("請在修改模式下使用此功能");
        }
    }

    /// <summary>
    /// 清單轉換角色可否戰鬥
    /// </summary>
    /// <param name="index">性別</param>
    public void CharacterBattleDropDownIndexChange(int index)
    {
        if (PlantManager.Instance.EditorMode == PlantManager.eEditorMode.Edit)
        {
            PlantManager.Instance.CurrentNode.CanBattle = (index == 0) ? false : true;
            Debug.Log("更換可否戰鬥");
        }
        else
        {
            Debug.LogError("請在修改模式下使用此功能");
        }
    }

    /// <summary>
    /// 修改對話內容
    /// </summary>
    public void CharacterContentChange()
    {
        var inputtext = mContentInputText.text;
        mContentInputText.text = "";
        if (PlantManager.Instance.EditorMode == PlantManager.eEditorMode.Edit)
        {
            PlantManager.Instance.CurrentNode.Content = inputtext;
            Debug.Log(inputtext);
            Debug.Log("將此Content放入Node");
            return;
        }
        else
        {
            Debug.LogError("請在修改模式下使用此功能");
        }
    }

    /// <summary>
    /// 更換流水號
    /// </summary>
    public void CharacterSerialNumberChange()
    {
        var inputtext = mSerialNumberInputText.text;
        mSerialNumberInputText.text = "";
        int serialnumber;
        var isint = int.TryParse(inputtext, out serialnumber);
        if (PlantManager.Instance.EditorMode == PlantManager.eEditorMode.Edit)
        {
            if (!isint)
            {
                Debug.LogError("請輸入數字");
                return;
            }
        }
        PlantManager.Instance.CurrentNode.SerialNumber = int.Parse(inputtext);
        ChangeSerialNumber();
    }

    /// <summary>
    /// 讀取BinaryData(Excel版)
    /// </summary>
    public void LoadInteractBinaryData()
    {
        mPath = URLManager.GetURL(AssetBundleManager.Instance.PlayModePlatform, "NPCdb", SaveFileFormat.SaveFromat(eSaveFileFormat.txt));
        SaveLoad load = new SaveLoad(SaveLoad.eSystemType.NPCInterct);
        load.Load(mPath);
    }

    /// <summary>
    /// 將顯示文字重新偵測
    /// </summary>
    public override void SetEditorValue()
    {
        base.SetEditorValue();
        ChangeSexValue();
        ChangeSerialNumber();
    }

    /// <summary>
    /// 流水號顯示
    /// </summary>
    void ChangeSerialNumber()
    {
        mCurrentSerialText.text = "目前流水號 : " + PlantManager.Instance.CurrentNode.SerialNumber;
    }

    /// <summary>
    /// 性別顯示
    /// </summary>
    void ChangeSexValue()
    {
        mCurrentSexText.text = "目前性別 : " + ((PlantManager.Instance.CurrentNode.Sex == 0) ? "男" : "女");
    }

    void ChangeBattleValue()
    {
        mCurrentSexText.text = "戰鬥 : " + ((PlantManager.Instance.CurrentNode.CanBattle == false) ? "不可" : "可");
    }
}
