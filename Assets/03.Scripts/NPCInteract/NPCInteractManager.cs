﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCInteractManager : MonoBehaviour
{
    void Start()
    {
        LoadNpcData();
    }

    void LoadNpcData()
    {
        var load = new SaveLoad(SaveLoad.eSystemType.NPCInterct);
        string npcpath;
        // 隱藏Http讀取模式
        //if (ObjectPoolManager.Instance.SystemType == ObjectPoolManager.eSystemType.BattleSystem)
        //{
        //    npcpath = URLManager.GetURL(AssetBundleManager.Instance.PlayModePlatform, "NPCBattledb", SaveFileFormat.SaveFromat(eSaveFileFormat.txt));
        //}
        //else
        //{
        //    npcpath = URLManager.GetURL(AssetBundleManager.Instance.PlayModePlatform, "NPCdb", AssetBundleManager.Instance.Version, SaveFileFormat.SaveFromat(eSaveFileFormat.txt));
        //}
        ObjectPoolManager.Instance.ReSetCharacter();
        npcpath = URLManager.GetProjectFolderPath() + "/NPCdb";
        load.Load(npcpath);
    }
}
