﻿using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine;
using System.Collections.Generic;

public class SaveLoadInteractManager : ISaveLoadInterface
{
    public void Save(string path, string name)
    {
        SaveBinary(path, name);
    }

    public void Load(string path)
    {
        // 隱藏http讀取模式
        //SaveLoadPlant.Instance.StartCoroutine(DownloadData(path));
        using (var stream = new FileStream(path + SaveFileFormat.SaveFromat(eSaveFileFormat.txt), FileMode.Open))
        {
            ReadbyteByHeaderFile(stream);
        }
    }

    IEnumerator DownloadData(string path)
    {
        var request = UnityWebRequest.Get(path);
        yield return request.SendWebRequest();
        if (request.isDone)
        {
            Debug.Log("加載完成");
            if (LoadAssetBundleUI.Instance.mLoadingText != null)
            {
                LoadAssetBundleUI.Instance.mLoadingText.text = "加載完成";
                ObjectPoolManager.Instance.ClearObjectInGame();
                yield return null;
            }
        }

        do
        {
            Debug.Log("等待Asset Bundle載入完成");
            yield return new WaitForSeconds(0.5f);
        }
        while (!AssetBundleManager.Instance.LoadAssetDone);

        ObjectPoolManager.Instance.ReSetCharacter();
        switch (ObjectPoolManager.Instance.SystemType)
        {
            case ObjectPoolManager.eSystemType.BattleSystem:
                ReadbyteByHeader(request);
                break;
            default:
                Debug.Log("自動閱讀");
                ReadBinaryAuto(request);
                break;
        }
    }

    void ReadBinaryAuto(UnityWebRequest request)
    {
        BinaryFormatter binaryformatter = new BinaryFormatter();
        using (Stream streamdata = new MemoryStream(request.downloadHandler.data))
        {
            InteractNPCData data = binaryformatter.Deserialize(streamdata) as InteractNPCData;
            SaveLoadPlant.Instance.SetNPCInteractLoadData(data.MonsterData, ResourceLoad.Instance.AddInteractable);
        }
    }

    void ReadbyteByHeaderFile(Stream readStream)
    {
        BinaryReader reader = new BinaryReader(readStream);
        List<InteractNPCData.sMonsterData> monsterdata = new List<InteractNPCData.sMonsterData>();
        while ((reader.PeekChar() != -1))
        {
            monsterdata.Add(ReadData(reader));
        }
        SaveLoadPlant.Instance.SetNPCInteractLoadData(monsterdata, ResourceLoad.Instance.AddInteractable);
        Debug.Log("讀取結束");
    }

    void ReadbyteByHeader(UnityWebRequest request)
    {
        using (MemoryStream streamdata = new MemoryStream(request.downloadHandler.data))
        {
            BinaryReader reader = new BinaryReader(streamdata);
            List<InteractNPCData.sMonsterData> monsterdata = new List<InteractNPCData.sMonsterData>();
            while ((reader.PeekChar() != -1))
            {
                monsterdata.Add(ReadData(reader));
            }
            SaveLoadPlant.Instance.SetNPCInteractLoadData(monsterdata, ResourceLoad.Instance.AddInteractable);
        }
        Debug.Log("讀取結束");
    }

    InteractNPCData.sMonsterData ReadData(BinaryReader reader)
    {
        InteractNPCData.sMonsterData data = new InteractNPCData.sMonsterData();
        data.MonsterNumber = new int[BattleManager.MAX_CHARACTER];
        int count;
        for (int typelength = 0; typelength < InteractNPCData.TYPE_LENGTH; typelength++)
        {
            count = reader.ReadInt32();
            switch (count)
            {
                case 1:
                    int theint = reader.ReadInt32();
                    data = AddToInteractNPCData(typelength, theint, data);
                    break;
                case 2:
                    float thefloat = reader.ReadSingle();
                    data = AddToInteractNPCData(typelength, thefloat, data);
                    break;
                case 3:
                    bool[] thebool = ReadBit(reader.ReadByte());
                    data = AddToInteractNPCData(typelength, thebool, data);
                    break;
                case 4:
                    string thestring = reader.ReadString();
                    data = AddToInteractNPCData(typelength, thestring, data);
                    break;
            }
        }
        return data;
    }

    bool[] ReadBit(byte readByte)
    {
        var bitarray = new BitArray(BitConverter.GetBytes(readByte));
        bool[] bitbool = new bool[8];
        for (int boollength = 0; boollength < bitbool.Length; boollength++)
        {
            bitbool[boollength] = bitarray[boollength];
        }
        return bitbool;
    }

    void SaveBinary(string path, string name)
    {
        var data = new InteractNPCData();
        using (var stream = new FileStream(path + name + SaveFileFormat.SaveFromat(eSaveFileFormat.txt), FileMode.Create))
        {
            Debug.Log("存檔");
            using (BinaryWriter binarywriter = new BinaryWriter(stream))
            {
                foreach (InteractNPCData.sMonsterData thedata in data.MonsterData)
                {
                    BinaryDataWriter.WriteBinary(binarywriter, thedata.SaveRotate);
                    BinaryDataWriter.WriteBinary(binarywriter, thedata.YAxis);
                    BinaryDataWriter.WriteBinary(binarywriter, thedata.SerialNumber);
                    BinaryDataWriter.WriteBinary(binarywriter, thedata.NPCName);
                    BinaryDataWriter.WriteBinary(binarywriter, thedata.NPCNumber);
                    BinaryDataWriter.WriteBinary(binarywriter, thedata.Sex);
                    BinaryDataWriter.WriteBinary(binarywriter, thedata.Content);
                    bool[] bytebool = { thedata.CanBattle, thedata.CanTalk };
                    BinaryDataWriter.WriteBinary(binarywriter, bytebool);
                    BinaryDataWriter.WriteBinary(binarywriter, thedata.MonsterNumber[0]);
                    BinaryDataWriter.WriteBinary(binarywriter, thedata.MonsterNumber[1]);
                    BinaryDataWriter.WriteBinary(binarywriter, thedata.MonsterNumber[2]);
                    BinaryDataWriter.WriteBinary(binarywriter, thedata.MonsterNumber[3]);
                    BinaryDataWriter.WriteBinary(binarywriter, thedata.MonsterNumber[4]);
                    BinaryDataWriter.WriteBinary(binarywriter, thedata.MonsterNumber[5]);
                    BinaryDataWriter.WriteBinary(binarywriter, thedata.Boundx);
                    BinaryDataWriter.WriteBinary(binarywriter, thedata.Boundy);
                }

            }
        }
    }

    InteractNPCData.sMonsterData AddToInteractNPCData(int value, int arrayValue, InteractNPCData.sMonsterData data)
    {
        switch (value)
        {
            case 2:
                data.SerialNumber = arrayValue;
                break;
            case 4:
                data.NPCNumber = arrayValue;
                break;
            case 5:
                data.Sex = arrayValue;
                break;
            case 8:
                data.MonsterNumber[0] = arrayValue;
                break;
            case 9:
                data.MonsterNumber[1] = arrayValue;
                break;
            case 10:
                data.MonsterNumber[2] = arrayValue;
                break;
            case 11:
                data.MonsterNumber[3] = arrayValue;
                break;
            case 12:
                data.MonsterNumber[4] = arrayValue;
                break;
            case 13:
                data.MonsterNumber[5] = arrayValue;
                break;
            case 14:
                data.Boundx = arrayValue;
                break;
            case 15:
                data.Boundy = arrayValue;
                break;
        }

        return data;
    }

    InteractNPCData.sMonsterData AddToInteractNPCData(int value, float arrayValue, InteractNPCData.sMonsterData data)
    {
        switch (value)
        {
            case 0:
                data.SaveRotate = arrayValue;
                break;
            case 1:
                data.YAxis = arrayValue;
                break;
        }
        return data;
    }

    InteractNPCData.sMonsterData AddToInteractNPCData(int value, bool[] arrayValue, InteractNPCData.sMonsterData data)
    {
        switch (value)
        {
            case 7:
                data.CanBattle = arrayValue[0];
                data.CanTalk = arrayValue[1];
                break;
        }
        return data;
    }

    InteractNPCData.sMonsterData AddToInteractNPCData(int value, string arrayValue, InteractNPCData.sMonsterData data)
    {
        switch (value)
        {
            case 3:
                data.NPCName = arrayValue;
                break;
            case 6:
                data.Content = arrayValue;
                break;
        }
        return data;
    }
}

/// <summary>
/// 儲存檔案
/// 類型 : NPCInteract
/// </summary>
[Serializable]
public class InteractNPCData
{
    public struct sMonsterData
    {
        public float SaveRotate;
        public float YAxis;
        public int SerialNumber;
        public string NPCName;
        public int NPCNumber;
        public int Sex;
        public string Content;
        public int[] MonsterNumber;
        public bool CanBattle;
        public bool CanTalk;
        public int Boundx;
        public int Boundy;

        public sMonsterData(float saverotate, float yaxis, int serialNumber, string npcName, int npcNumber, int sex, string content, bool canBattle, int[] monsterNumber, bool cantalk, int boundx, int boundy)
        {
            SaveRotate = saverotate;
            YAxis = yaxis;
            SerialNumber = serialNumber;
            NPCName = npcName;
            NPCNumber = npcNumber;
            Sex = sex;
            Content = content;
            MonsterNumber = monsterNumber;
            CanBattle = canBattle;
            CanTalk = cantalk;
            Boundx = boundx;
            Boundy = boundy;
            if (Content == null)
            {
                Content = " ";
            }
            if (NPCName == null)
            {
                NPCName = " ";
            }
        }
    }

    public const int TYPE_LENGTH = 16;

    public List<sMonsterData> MonsterData = new List<sMonsterData>();

    public InteractNPCData()
    {
        for (var boundx = 0; boundx < Grids.Instance.Bound; boundx++)
        {
            for (var boundy = 0; boundy < Grids.Instance.Bound; boundy++)
            {
                if (Grids.Instance.NodeArray[boundx, boundy].HasGameObject != null)
                {
                    Node node = Grids.Instance.NodeArray[boundx, boundy];
                    sMonsterData data = new sMonsterData(node.Rotation, node.HasGameObject.transform.position.y, node.SerialNumber, node.HasGameObject.name, node.CharacterNumber, node.Sex, node.Content, node.CanBattle, node.CharacterBattleNumber, node.CanTalk, boundx, boundy);
                    MonsterData.Add(data);
                }
            }
        }
    }
}
