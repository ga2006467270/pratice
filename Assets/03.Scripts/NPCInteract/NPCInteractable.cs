﻿using UnityEngine;
using UnityEngine.EventSystems;

public class NPCInteractable : CharacterInteractable
{
    int mNodePosx;
    int mNodePosy;
    [SerializeField] LayerMask mPlayerLayer = 1 << 10;
    /// <summary>
    /// 驚嘆號位置 根據每個模組高度不同來設定
    /// </summary>
    [SerializeField] float mMarkOffset = 2.5f;
    bool mShowMark;
    GameObject mExplanationMark;

    public void Interactable()
    {
        Node npcnode = Grids.Instance.NodeArray[mNodePosx, mNodePosy];
        if (npcnode.CanTalk)
        {
            ShowTalkUI(npcnode.CanBattle);
        }
    }

    public void SetBound(int nodePosx, int nodePosy)
    {
        mNodePosx = nodePosx;
        mNodePosy = nodePosy;
    }

    void Start()
    {
        mExplanationMark = Instantiate(ObjectPoolManager.Instance.GetObjectFromPool("ExplanationMark"), transform.position + new Vector3(0, mMarkOffset, 0), transform.rotation);
        mExplanationMark.SetActive(false);
    }

    void Update()
    {
        InteractDected();
    }

    /// <summary>
    /// Warning "Invalid texture used for cursor - check importer settings" appears
    /// Fixed in 2019.1.0a : https://issuetracker.unity3d.com/issues/cursor-dot-setcursor-prints-invalid-texture-used-warning-when-the-texture-is-loaded-with-assetbundles
    /// Asset Bundle匯入Texture 用在Cursor上會有TextureType警告 已於2019.1.0a修正
    /// </summary>
    void OnMouseEnter()
    {
        if (!EventSystem.current.IsPointerOverGameObject())
        {

            Texture2D cursortex = ObjectPoolManager.Instance.GetObjectFromTextPool("NpcInteractMark");
            Cursor.SetCursor(cursortex, Vector2.zero, mCursorMode);
        }
    }

    void OnMouseExit()
    {
        Cursor.SetCursor(null, Vector2.zero, mCursorMode);
    }

    void OnMouseDown()
    {
        if (!EventSystem.current.IsPointerOverGameObject())
        {
            if (mIsInteractableRange)
            {
                Interactable();
            }
            else
            {
                Debug.LogWarning("距離NPC太遠");
            }
        }
    }

    void InteractDected()
    {
        if (mExplanationMark != null)
        {
            if (Physics.OverlapSphere(transform.position, mDetectRadius, mPlayerLayer).Length > 0)
            {
                mIsInteractableRange = true;
                if (!mShowMark)
                {
                    ShowMark();
                }
            }
            else
            {
                mShowMark = false;
                mExplanationMark.SetActive(false);
                mIsInteractableRange = false;
            }
        }
    }

    void ShowTalkUI(bool CanBattle)
    {
        Node currentNode = Grids.Instance.NodeArray[mNodePosx, mNodePosy];
        NPCTalkUI.Instance.ShowUI(gameObject, mDetectRadius, currentNode.HasGameObject.name, currentNode.Sex, currentNode.Content, currentNode.CanBattle);
        if (CanBattle)
        {
            Debug.LogWarning("可以戰鬥");
            CharacterStats[] monsterscharacterstats = new CharacterStats[BattleManager.MAX_CHARACTER];
            for (int nodemonsternumber = 0; nodemonsternumber < BattleManager.MAX_CHARACTER; nodemonsternumber++)
            {
                MobStats.sMonsterStats monsterstats = MonsterdbDic.Instance.GetMonsterIntDic(currentNode.CharacterBattleNumber[nodemonsternumber]);
                monsterscharacterstats[nodemonsternumber] = new CharacterStats(monsterstats.Attack, monsterstats.Defend, monsterstats.MaxHp, currentNode.CharacterBattleNumber[nodemonsternumber], BattleManager.Instance.EnemyNode[nodemonsternumber], "Monster");
            }
            BattleManager.Instance.SetEnemyStats(monsterscharacterstats);
            //NPCTalkUI.Instance.CanBattleUI();
        }
    }

    void ShowMark()
    {
        mExplanationMark.SetActive(true);
        mShowMark = true;
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        if (mIsInteractableRange)
        {
            Gizmos.color = Color.red;
        }
        Gizmos.DrawWireSphere(transform.position, mDetectRadius);
    }
}
