﻿using System.IO;
/// <summary>
/// 存讀檔工廠
/// </summary>
public class SaveLoad
{
    public enum eSystemType
    {
        PlantNPC,
        Excel,
        NPCInterct,
        AssetBundle,
        Battle,
    }

    eSystemType mSystemType;
    SaveStore mFactory;

    public SaveLoad(eSystemType systemType)
    {
        mSystemType = systemType;
    }

    public void Save(string path, string name)
    {
        mFactory = new SaveStore(new SimpleSaveFactory());
        mFactory.SaveOrder(mSystemType, path, name);
    }

    public void Load(string path)
    {
        mFactory = new SaveStore(new SimpleSaveFactory());
        mFactory.LoadOrder(mSystemType, path);
    }

    public static void WriteBinary(BinaryWriter writer, int content)
    {
        int value = 1;
        writer.Write(value);
        writer.Write(content);
    }

    public static void WriteBinary(BinaryWriter writer, float content)
    {
        int value = 2;
        writer.Write(value);
        writer.Write(content);
    }

    public static void WriteBinary(BinaryWriter writer, bool content)
    {
        int value = 3;
        writer.Write(value);
        writer.Write(content);
    }

    public static void WriteBinary(BinaryWriter writer, string content)
    {
        int value = 4;
        writer.Write(value);
        writer.Write(content);
    }
}

public class SaveStore
{
    SimpleSaveFactory mFactory;

    public SaveStore(SimpleSaveFactory factory)
    {
        mFactory = factory;
    }

    public void SaveOrder(SaveLoad.eSystemType saveEnum, string path, string name)
    {
        ISaveLoadInterface savetype;
        savetype = mFactory.CreateResource(saveEnum);
        savetype.Save(path, name);
    }

    public void LoadOrder(SaveLoad.eSystemType saveEnum, string path)
    {
        ISaveLoadInterface savetype;
        savetype = mFactory.CreateResource(saveEnum);
        savetype.Load(path);
    }
}

public class SimpleSaveFactory
{
    public ISaveLoadInterface CreateResource(SaveLoad.eSystemType resourceType)
    {
        switch (resourceType)
        {
            case SaveLoad.eSystemType.PlantNPC:
                return new SaveLoadPlantManager();
            case SaveLoad.eSystemType.Excel:
                return new SaveLoadExcelsManager();
            case SaveLoad.eSystemType.NPCInterct:
                return new SaveLoadInteractManager();
            case SaveLoad.eSystemType.AssetBundle:
                return new AssetBundleDataListManager();
            case SaveLoad.eSystemType.Battle:
                return new BattleStatsSave();
        }
        return null;
    }
}
