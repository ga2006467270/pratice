﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterStats
{
    public float AttackValue
    {
        get
        {
            return mAttackValue;
        }

        set
        {
            mAttackValue = value;
        }
    }

    public float DefendValue
    {
        get
        {
            return mDefendValue;
        }

        set
        {
            mDefendValue = value;
        }
    }

    public float MaxHp
    {
        get
        {
            return mMaxHp;
        }

        set
        {
            mMaxHp = value;
        }
    }

    public float CurrentHp
    {
        get
        {
            return mCurrentHp;
        }

        set
        {
            mCurrentHp = value;
            if (mCurrentHp <= 0)
            {
                Debug.Log(mParentNode.Character.name + " 死亡");
                mParentNode.CharacterDown();
            }
        }
    }

    public int CharacterNumber
    {
        get
        {
            return mCharacterNumber;
        }

        set
        {
            mCharacterNumber = value;
        }
    }

    public string Name
    {
        get
        {
            return mName;
        }
    }

    float mAttackValue = 0;
    float mDefendValue = 0;
    float mMaxHp = 0;
    float mCurrentHp = 0;
    int mCharacterNumber = 0;
    string mName;
    BattleNode mParentNode;

    public CharacterStats(float attack, float defend, float maxhp, int characternumber, BattleNode parentNode, string name)
    {
        mAttackValue = attack;
        mDefendValue = defend;
        mMaxHp = maxhp;
        mCurrentHp = mMaxHp;
        mCharacterNumber = characternumber;
        mParentNode = parentNode;
        mName = name;
    }

    public CharacterStats()
    {
    }
}
