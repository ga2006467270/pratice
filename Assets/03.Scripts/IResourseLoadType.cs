﻿using UnityEngine;

public interface IResourseLoadType
{
    void LoadResourses();
}

public class AddResources
{
    /// <summary>
    /// 讀取資料
    /// </summary>
    /// <param name="obejctName">物件名稱.</param>
    public static void LoadObjectResources(string obejctName)
    {
        var loadobject = Resources.Load<GameObject>(obejctName);
        if (loadobject == null)
        {
            return;
        }
        ObjectPoolManager.Instance.AddObject(loadobject, obejctName);
    }

    public static void LoadTextureResources(string obejctName)
    {
        var texture = Resources.Load(obejctName) as Texture2D;
        ObjectPoolManager.Instance.AddText(texture, texture.name);
    }

    public static void LoadMaterial(string materialName)
    {
        var material = Resources.Load(materialName) as Material;
        ObjectPoolManager.Instance.AddMaterial(material, material.name);
    }
}

public class ResourceLoadMove : IResourseLoadType
{
    void IResourseLoadType.LoadResourses()
    {
        AddResources.LoadObjectResources("MainCamera");
        AddResources.LoadObjectResources("DirectionalLight");
        AddResources.LoadObjectResources("EventSystem");
        AddResources.LoadObjectResources("Enviroment");
    }
}

public class ResourceLoadPlantNPCSave : IResourseLoadType
{
    void IResourseLoadType.LoadResourses()
    {
        ObjectPoolManager.Instance.ReSetCharacter();

        AddResources.LoadObjectResources("MainCamera");
        AddResources.LoadObjectResources("DirectionalLight");
        AddResources.LoadObjectResources("EventSystem");
        AddResources.LoadObjectResources("Enviroment");
    }
}

public class ResourceLoadNPCInteract : IResourseLoadType
{
    void IResourseLoadType.LoadResourses()
    {
        ObjectPoolManager.Instance.ReSetCharacter();

        AddResources.LoadObjectResources("MainCamera");
        AddResources.LoadObjectResources("DirectionalLight");
        AddResources.LoadObjectResources("EventSystem");
        AddResources.LoadObjectResources("Enviroment");
    }
}

public class ResourceLoadNPCInteractPlant : IResourseLoadType
{
    void IResourseLoadType.LoadResourses()
    {
        ObjectPoolManager.Instance.ReSetCharacter();

        AddResources.LoadObjectResources("MainCamera");
        AddResources.LoadObjectResources("DirectionalLight");
        AddResources.LoadObjectResources("EventSystem");
        AddResources.LoadObjectResources("Enviroment");
    }
}

public class ResourceLoadAssetBundle : IResourseLoadType
{
    public void LoadResourses()
    {
        AddResources.LoadObjectResources("MainCamera");
        AddResources.LoadObjectResources("DirectionalLight");
        AddResources.LoadObjectResources("EventSystem");
        AddResources.LoadObjectResources("Enviroment");
    }
}
