﻿using UnityEngine;

public class SaveLoadStructure
{
    protected static int RowNumber
    {
        get
        {
            mRowNumber++;
            return mRowNumber - 1;
        }
        set
        {
            mRowNumber = value;
        }
    }

    static int mRowNumber;

    public struct sGeneral
    {
        const int GENERAL_VALUE_LENGTH = 6;

        public int SerialNumber
        {
            get
            {
                return mSerialNumber;
            }
            set
            {
                mSerialNumber = value;
            }
        }
        public int GeneralNumber
        {
            get
            {
                return mGeneralNumber;
            }
            set
            {
                mGeneralNumber = value;
            }
        }

        public string Name
        {
            get
            {
                return mName;
            }
            set
            {
                mName = value;
            }
        }

        public int Attack
        {
            get
            {
                return mAttack;
            }
            set
            {
                mAttack = value;
            }
        }

        public int Armor
        {
            get
            {
                return mArmor;
            }
            set
            {
                mArmor = value;
            }
        }

        public int MoveSpeed
        {
            get
            {
                return mMoveSpeed;
            }
            set
            {
                mMoveSpeed = value;
            }
        }

        int mSerialNumber;
        int mGeneralNumber;
        string mName;
        int mAttack;
        int mArmor;
        int mMoveSpeed;

        public sGeneral InsertStruct(string[,,] logArray, int row)
        {
            if ((logArray.GetUpperBound(ROW) + UPPER_BOUND_OFFSET) != GENERAL_VALUE_LENGTH)
            {
                Debug.LogError("資料型態不符");
                return new sGeneral();
            }
            else
            {
                RowNumber = 0;
                sGeneral general = new sGeneral();
                general.SerialNumber = int.Parse(logArray[TABLE, row, RowNumber]);
                general.GeneralNumber = int.Parse(logArray[TABLE, row, RowNumber]);
                general.Name = logArray[TABLE, row, RowNumber];
                general.Attack = int.Parse(logArray[TABLE, row, RowNumber]);
                general.Armor = int.Parse(logArray[TABLE, row, RowNumber]);
                general.MoveSpeed = int.Parse(logArray[TABLE, row, RowNumber]);
                Debug.Log("SerialNumber = " + general.SerialNumber);
                Debug.Log("GeneralNumber = " + general.GeneralNumber);
                Debug.Log("Name = " + general.Name);
                Debug.Log("Attack = " + general.Attack);
                Debug.Log("Armor = " + general.Armor);
                Debug.Log("MoveSpeed = " + general.MoveSpeed);
                return general;
            }
        }
    }

    public struct sItem
    {
        const int ITEM_VALUE_LENGTH = 5;

        public int SerialNumber
        {
            get
            {
                return mSerialNumber;
            }
            set
            {
                mSerialNumber = value;
            }
        }

        public int ItemNumber
        {
            get
            {
                return mItemNumber;
            }
            set
            {
                mItemNumber = value;
            }
        }

        public string Name
        {
            get
            {
                return mName;
            }
            set
            {
                mName = value;
            }
        }

        public int Value
        {
            get
            {
                return mValue;
            }
            set
            {
                mValue = value;
            }
        }

        public string Description
        {
            get
            {
                return mDescription;
            }
            set
            {
                mDescription = value;
            }
        }

        int mSerialNumber;
        int mItemNumber;
        string mName;
        int mValue;
        string mDescription;

        public sItem InsertStruct(string[,,] logArray, int column)
        {
            if ((logArray.GetUpperBound(ROW) + UPPER_BOUND_OFFSET) != ITEM_VALUE_LENGTH)
            {
                Debug.LogError("資料型態不符");
                return new sItem();
            }
            else
            {
                RowNumber = 0;
                sItem item = new sItem();
                item.SerialNumber = int.Parse(logArray[TABLE, column, RowNumber]);
                item.ItemNumber = int.Parse(logArray[TABLE, column, RowNumber]);
                item.Name = logArray[TABLE, column, RowNumber];
                item.Value = int.Parse(logArray[TABLE, column, RowNumber]);
                item.Description = logArray[TABLE, column, RowNumber];
                Debug.Log("SerialNumber = " + item.SerialNumber);
                Debug.Log("ItemNumber = " + item.ItemNumber);
                Debug.Log("Name = " + item.Name);
                Debug.Log("Value = " + item.Value);
                Debug.Log("Description = " + item.Description);
                return item;
            }
        }
    }

    public struct sNPC
    {
        const int ITEM_VALUE_LENGTH = 7;

        public int SerialNumber
        {
            get
            {
                return mSerialNumber;
            }
            set
            {
                mSerialNumber = value;
            }
        }

        public int CharacterNumber
        {
            get
            {
                return mCharacterNumber;
            }
            set
            {
                mCharacterNumber = value;
            }
        }

        public string Name
        {
            get
            {
                return mName;
            }
            set
            {
                mName = value;
            }
        }

        public int Positionx
        {
            get
            {
                return mPositionx;
            }
            set
            {
                mPositionx = value;
            }
        }

        public int Positiony
        {
            get
            {
                return mPositiony;
            }
            set
            {
                mPositiony = value;
            }
        }

        public int Sex
        {
            get
            {
                return mSex;
            }
            set
            {
                mSex = value;
            }
        }

        public string Content
        {
            get
            {
                return mContent;
            }
            set
            {
                mContent = value;
            }
        }

        int mSerialNumber;
        int mCharacterNumber;
        string mName;
        int mPositionx;
        int mPositiony;
        int mSex;
        string mContent;

        public sNPC InsertStruct(string[,,] logArray, int column)
        {
            if ((logArray.GetUpperBound(ROW) + UPPER_BOUND_OFFSET) != ITEM_VALUE_LENGTH)
            {
                Debug.LogError("資料型態不符");
                return new sNPC();
            }
            else
            {
                RowNumber = 0;
                sNPC npc = new sNPC();
                npc.SerialNumber = int.Parse(logArray[TABLE, column, RowNumber]);
                npc.CharacterNumber = int.Parse(logArray[TABLE, column, RowNumber]);
                npc.Name = logArray[TABLE, column, RowNumber];
                npc.Positionx = int.Parse(logArray[TABLE, column, RowNumber]);
                npc.Positiony = int.Parse(logArray[TABLE, column, RowNumber]);
                npc.Sex = int.Parse(logArray[TABLE, column, RowNumber]);
                npc.Content = logArray[TABLE, column, RowNumber];
                Debug.Log("SerialNumber = " + npc.SerialNumber);
                Debug.Log("ItemNumber = " + npc.CharacterNumber);
                Debug.Log("Name = " + npc.Name);
                Debug.Log("positionx = " + npc.Positionx);
                Debug.Log("positiony = " + npc.Positiony);
                Debug.Log("Sex = " + npc.Sex);
                Debug.Log("Content = " + npc.Content);
                return npc;
            }
        }
    }

    const int TABLE = 0;
    const int COLUMN = 1;
    const int ROW = 2;
    const int UPPER_BOUND_OFFSET = 1;

    public static int Index = 0;
    public static string[,,] LogArray;

    /// <summary>
    /// 根據資料類型判斷轉換哪種資料型態
    /// </summary>
    /// <param name="index">The index.</param>
    /// <param name="logArray">excel資料</param>
    public static void IndexSearcher(int index, string[,,] logArray)
    {
        switch (index)
        {
            case 0:
                sItem[] items = new sItem[logArray.GetUpperBound(COLUMN) + UPPER_BOUND_OFFSET];
                for (int row = 0; row <= (logArray.GetUpperBound(COLUMN)); row++)
                {
                    items[row].InsertStruct(logArray, row);
                }
                break;
            case 1:
                sGeneral[] generals = new sGeneral[logArray.GetUpperBound(COLUMN) + UPPER_BOUND_OFFSET];
                for (int row = 0; row <= (logArray.GetUpperBound(COLUMN)); row++)
                {
                    generals[row].InsertStruct(logArray, row);
                }
                break;
            case 2:
                sNPC[] npc = new sNPC[logArray.GetUpperBound(COLUMN) + UPPER_BOUND_OFFSET];
                for (int row = 0; row <= (logArray.GetUpperBound(COLUMN)); row++)
                {
                    npc[row] = npc[row].InsertStruct(logArray, row);
                }
                SaveLoadPlant.Instance.CleanData();
                PlantManager.Instance.ExcelTurnToPlantNode(npc);
                break;
        }
    }
}
