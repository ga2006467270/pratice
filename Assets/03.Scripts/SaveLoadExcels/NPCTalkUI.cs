﻿using UnityEngine;
using UnityEngine.UI;

public class NPCTalkUI : MonoBehaviour
{
    public static NPCTalkUI Instance
    {
        get
        {
            if (mInstance == null)
            {
                GameObject gamemanager = new GameObject("NPCInteractCanvas");
                gamemanager.AddComponent<NPCTalkUI>();
            }
            return mInstance;
        }
    }

    static NPCTalkUI mInstance;
    [SerializeField] GameObject mNPCTalkeCanvas;
    [SerializeField] Text mNameText;
    [SerializeField] Text mContentText;
    [SerializeField] Button mBattleButton;
    GameObject mInteractObject;
    GameObject mPlayer;
    float mDetectRadius;

    /// <summary>
    /// 顯示對話UI 並修改數據為當前角色數據
    /// </summary>
    public void ShowUI(GameObject InteractObject, float detectRadius, string name, int sex, string talkContent,bool CanBattle)
    {
        mInteractObject = InteractObject;
        mDetectRadius = detectRadius;
        mNameText.color = (sex == 0 ? Color.blue : Color.red);
        mNameText.text = name;
        mContentText.text = talkContent;
        mNPCTalkeCanvas.SetActive(true);
        Debug.Log(CanBattle);
        if (CanBattle)
        {
            mBattleButton.gameObject.SetActive(true);
        }
        else
        {
            mBattleButton.gameObject.SetActive(false);
        }
    }

    // 待修正 人物資訊
    public void InBattle()
    {
        Debug.Log("進入戰鬥(UI)");
        BattleManager.Instance.Battle(BattleManager.Instance.EnemyNode);
        CloseUI();
    }

    /// <summary>
    /// 關閉UI
    /// </summary>
    public void CloseUI()
    {
        if (ObjectPoolManager.Instance.SystemType == ObjectPoolManager.eSystemType.BattleSystem)
        {
            mBattleButton.gameObject.SetActive(false);
        }
        mNPCTalkeCanvas.SetActive(false);
        mInteractObject = null;
    }

    void Awake()
    {
        mInstance = this;
    }

    void Start()
    {
        mPlayer = ObjectPoolManager.Instance.GetObjectInGame("Hero");
        mNPCTalkeCanvas.SetActive(false);
        if (ObjectPoolManager.Instance.SystemType == ObjectPoolManager.eSystemType.BattleSystem)
        {
            mBattleButton.gameObject.SetActive(false);
        }
    }

    void Update()
    {
        if (mInteractObject != null)
        {
            if ((mPlayer.transform.position - mInteractObject.transform.position).magnitude > mDetectRadius)
            {
                CloseUI();
            }
        }
    }
}
