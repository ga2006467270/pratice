﻿using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;

public class SaveLoadExcelsManager : ISaveLoadInterface
{
    /// <summary>
    /// 存取檔案
    /// </summary>
    /// <param name="path">檔案路徑</param>
    public void Save(string path, string name)
    {
        var binarayformatter = new BinaryFormatter();
        using (var stream = new FileStream(path + "/" + name + SaveFileFormat.SaveFromat(eSaveFileFormat.txt), FileMode.Create))
        {
            var data = new ExcelData(SaveLoadStructure.LogArray);
            binarayformatter.Serialize(stream, data);
        }
    }

    /// <summary>
    /// 讀取檔案
    /// </summary>
    /// <param name="path">檔案路徑</param>
    public void Load(string path)
    {
        if (path == "")
        {
            Debug.Log("你取消讀取了");
            return;
        }
        var binarayformatter = new BinaryFormatter();
        using (var stream = new FileStream(path, FileMode.Open))
        {
            var data = binarayformatter.Deserialize(stream) as ExcelData;
            SaveLoadStructure.IndexSearcher(SaveLoadStructure.Index, data.ExcelStringData);
        }
    }
}

[Serializable]
public class ExcelData
{
    public string[,,] ExcelStringData;

    public ExcelData(string[,,] itemLengh)
    {
        ExcelStringData = itemLengh;
    }
}
