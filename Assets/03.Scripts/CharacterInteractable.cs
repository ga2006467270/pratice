﻿using UnityEngine;

public abstract class CharacterInteractable : MonoBehaviour
{
    /// <summary>
    /// 物件偵測範圍 預設3f
    /// </summary>
    [SerializeField] protected float mDetectRadius = 3f;
    protected bool mIsInteractableRange;
    protected CursorMode mCursorMode = CursorMode.Auto;
}
