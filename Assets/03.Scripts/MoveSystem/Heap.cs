﻿using System;

public class Heap<T> where T : IHeapItem<T>
{
    T[] mItems;
    int mCurrentItemCount;

    public Heap(int maxHeapSize)
    {
        mItems = new T[maxHeapSize];
    }

    /// <summary>
    /// 取代List中的Remove，Remove後會將Heap陣列重新矲列整齊後，反傳新的FirstNode
    /// </summary>
    /// <returns>重新排列後的firstNode</returns>
    public T RemoveFirst()
    {
        T firstItem = mItems[0];
        mCurrentItemCount--;
        mItems[0] = mItems[mCurrentItemCount];
        mItems[0].HeapIndex = 0;
        SortDown(mItems[0]);
        return firstItem;
    }

    /// <summary>
    /// 如果你找到新的Path 在 CurrentList
    /// </summary>
    /// <param name="item">舊list</param>
    public void UpadateItem(T item)
    {
        SortUp(item);
    }

    public int Count
    {
        get
        {
            return mCurrentItemCount;
        }
    }

    /// <summary>
    /// 取代list中的Contains
    /// </summary>
    /// <param name="item">The item.</param>
    /// <returns>反傳Bool，是否Contains</returns>
    public bool Contains(T item)
    {
        return Equals(mItems[item.HeapIndex], item);
    }

    public void Add(T item)
    {
        item.HeapIndex = mCurrentItemCount;
        mItems[mCurrentItemCount] = item;
        SortUp(item);
        mCurrentItemCount++;
    }

    /// <summary>
    /// 一個物品要往下排列到屬於他的Cost的位置中
    /// </summary>
    /// <param name="item">The item.</param>
    void SortDown(T item)
    {
        while (true)
        {
            int childIndexLeft = item.HeapIndex * 2 + 1;
            int childIndexRight = item.HeapIndex * 2 + 2;

            int swapIndex = 0;
            if (childIndexLeft < mCurrentItemCount)
            {
                swapIndex = childIndexLeft;
                if (childIndexRight < mCurrentItemCount)
                {
                    if (mItems[childIndexLeft].CompareTo(mItems[childIndexRight]) < 0)
                    {
                        swapIndex = childIndexRight;
                    }
                }

                if (item.CompareTo(mItems[swapIndex]) < 0)
                {
                    Swap(item, mItems[swapIndex]);
                }
                else
                {
                    return;
                }
            }
            else
            {
                return;
            }
        }
    }

    /// <summary>
    /// 將一個位置的Item往上
    /// </summary>
    /// <param name="item">The item.</param>
    void SortUp(T item)
    {
        int parentIndex = (item.HeapIndex - 1) / 2;

        while (true)
        {
            T parenItem = mItems[parentIndex];
            if (item.CompareTo(parenItem) > 0)
            {
                Swap(item, parenItem);
            }
            else
            {
                break;
            }

            parentIndex = (item.HeapIndex - 1) / 2;
        }
    }

    /// <summary>
    /// 兩個Item之間互換
    /// </summary>
    /// <param name="itemA">交換者.</param>
    /// <param name="itemB">被交換者</param>
    void Swap(T itemA, T itemB)
    {
        mItems[itemA.HeapIndex] = itemB;
        mItems[itemB.HeapIndex] = itemA;
        int itemAIndex = itemA.HeapIndex;
        itemA.HeapIndex = itemB.HeapIndex;
        itemB.HeapIndex = itemAIndex;
    }
}

/// <summary>
/// 進來的Heap必須實作IHeapItem以及CompareTo
/// </summary>
/// <typeparam name="T">任何</typeparam>
public interface IHeapItem<T> : IComparable<T>
{
    int HeapIndex
    {
        get;
        set;
    }
}
