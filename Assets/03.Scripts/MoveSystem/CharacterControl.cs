﻿using UnityEngine;

public abstract class CharacterControl : MonoBehaviour
{
    public float MoveSpeed
    {
        get
        {
            return mMoveSpeed;
        }
        set
        {
            mMoveSpeed = value;
        }
    }

    public bool IsMoving
    {
        get
        {
            return mKeepMove;
        }
        set
        {
            mKeepMove = value;
        }
    }

    [SerializeField] float mMoveSpeed = 10f;
    bool mKeepMove;

    public virtual void Move()
    {
    }
}
