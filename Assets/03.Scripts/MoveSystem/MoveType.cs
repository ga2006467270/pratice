﻿using UnityEngine;

/// /// <summary>
/// 移動方式，需有移動物件/目的地
/// </summary>
interface IMoveType
{
    void Move(Transform moveObject);
}