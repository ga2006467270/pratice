﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// 點擊移動，同時偵測是否有Joystick移動，若有則直接跳出Function
/// </summary>
class MoveByClickDetect : IMoveType
{
    const float STOP_DISTANCE = 0.015F;
    const float ACCEPT_NEXT_PATH_COUNT = 0.08F;

    List<Node> mPath = new List<Node>();
    CharacterControl mCharacter;
    VirtualJoyStick mJoystickDetected;
    float mMoveSpeed;
    bool mUIClick;
    float mCanAcceptNextPath;

    public MoveByClickDetect(float moveSpeed, CharacterControl character)
    {
        mMoveSpeed = moveSpeed;
        mCharacter = character;
        mJoystickDetected = ObjectPoolManager.Instance.GetObjectInGame("JoyStickCanvas").GetComponentInChildren<VirtualJoyStick>();
    }

    public void Move(Transform moveObject)
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        int mask = 1 << LayerMask.NameToLayer("Ground");
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, mask) && mCanAcceptNextPath > ACCEPT_NEXT_PATH_COUNT)
        {
            GridDetectHelper.Instance.GridDected(hit.point);
            if ((Input.GetMouseButton(0) || (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)) && hit.collider.gameObject.layer == (LayerMask.NameToLayer("Ground")))
            {
                if (!IsPointerOverUIObject())
                {
                    mPath = PathFinding.Instance.FindPath(moveObject.transform.position, hit.point);
                    mCanAcceptNextPath = 0f;
                }
            }
        }
        if (mPath.Count > 0)
        {
            if (mJoystickDetected != null)
            {
                if (mJoystickDetected.JoyStickDirection != Vector3.zero)
                {
                    mPath.Clear();
                    return;
                }
            }
            mCharacter.IsMoving = true;
            Vector3 moveDirection = mPath[0].Pos - moveObject.transform.position;
            moveDirection = new Vector3(moveDirection.x, 0, moveDirection.z);
            moveObject.transform.position += Vector3.Normalize(moveDirection) * Time.deltaTime * mMoveSpeed;
            Vector3 movePathMagnitude = new Vector3(mPath[0].Pos.x, moveObject.transform.position.y, mPath[0].Pos.z);
            if ((moveObject.transform.position - movePathMagnitude).magnitude < mMoveSpeed * STOP_DISTANCE)
            {
                moveObject.transform.position = new Vector3(mPath[0].Pos.x, moveObject.transform.position.y, mPath[0].Pos.z);
                mPath.Remove(mPath[0]);
            }
            else
            {
                moveObject.transform.rotation = Quaternion.LookRotation(new Vector3(moveDirection.x, 0, moveDirection.z));
            }
        }
        if (mPath.Count <= 0)
        {
            mCharacter.IsMoving = false;
        }
        mCanAcceptNextPath += Time.deltaTime;
    }
    bool IsPointerOverUIObject()
    {
        PointerEventData eventdatacurrentposition = new PointerEventData(EventSystem.current);
        eventdatacurrentposition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventdatacurrentposition, results);
        return results.Count > 0;
    }

}