﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class VirtualJoyStick : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler
{
    public static VirtualJoyStick Instance
    {
        get
        {
            if (mInstance == null)
            {
                var gamemanager = new GameObject("VirtualJoyStick");
                gamemanager.AddComponent<VirtualJoyStick>();
            }
            return mInstance;
        }
    }

    static VirtualJoyStick mInstance;

    public Vector3 JoyStickDirection;

    [SerializeField] Image mBgImg;
    [SerializeField] Image mJoystickImg;
    Vector2 mJoystickoriginalimg;

    public void OnDrag(PointerEventData ped)
    {
        Vector2 pos;
        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(mBgImg.rectTransform, ped.position, ped.pressEventCamera, out pos))
        {
            float posx = Mathf.Clamp(pos.x / mBgImg.rectTransform.sizeDelta.x / 2, -1, 1);
            float posy = Mathf.Clamp(pos.y / mBgImg.rectTransform.sizeDelta.y / 2, -1, 1);
            JoyStickDirection = Vector3.Normalize(new Vector3(posx, 0, posy));
            mJoystickImg.rectTransform.anchoredPosition = new Vector2(Mathf.Clamp(posx * mBgImg.rectTransform.sizeDelta.x, -mBgImg.rectTransform.sizeDelta.x * mJoystickImg.rectTransform.localScale.x, mBgImg.rectTransform.sizeDelta.x * mJoystickImg.rectTransform.localScale.x), Mathf.Clamp(posy * mBgImg.rectTransform.sizeDelta.y, -mBgImg.rectTransform.sizeDelta.y * mJoystickImg.rectTransform.localScale.y, mBgImg.rectTransform.sizeDelta.y * mJoystickImg.rectTransform.localScale.y));
        }
    }

    public void OnPointerDown(PointerEventData ped)
    {
        OnDrag(ped);
        PlayerControl.Instance.JoyStickIsMove = true;
    }

    public void OnPointerUp(PointerEventData ped)
    {
        OnDrag(ped);
        PlayerControl.Instance.JoyStickIsMove = false;
        PlayerControl.Instance.IsMoving = false;
        mJoystickImg.rectTransform.position = mJoystickoriginalimg;
        JoyStickDirection = Vector3.zero;
    }

    void Awake()
    {
        mInstance = this;
    }

    /// <summary>
    /// 找到JoyStick圖片
    /// </summary>
    void Start()
    {
        mJoystickoriginalimg = mJoystickImg.rectTransform.position;
    }
}
