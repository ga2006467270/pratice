﻿using System.Collections.Generic;
using UnityEngine;

public class PathFinding : MonoBehaviour
{
    const int STRAIGHT_COST = 10;
    const int SLASH_COST = 14;

    public static PathFinding Instance
    {
        get
        {
            if (mInstance == null)
            {
                GameObject gamemanager = new GameObject("PathFinding");
                gamemanager.AddComponent<PathFinding>();
            }
            return mInstance;
        }
    }

    static PathFinding mInstance;

    /// <summary>
    /// 玩家與目標點的距離
    /// </summary>
    /// <param name="destination">目標點</param>
    public List<Node> FindPath(Vector3 player, Vector3 destination)
    {
        Node startnode = Grids.Instance.NodeFromWorldPoint(player);
        Node targetnode = Grids.Instance.NodeFromWorldPoint(destination);
        Heap<Node> countnode = new Heap<Node>(Grids.Instance.MaxSize);
        HashSet<Node> discountnode = new HashSet<Node>();
        if (!targetnode.Walkable)
        {
            return new List<Node>();
        }
        countnode.Add(startnode);

        while (countnode.Count > 0)
        {
            Node currentnode = countnode.RemoveFirst();
            discountnode.Add(currentnode);

            if (currentnode == targetnode)
            {
                List<Node> finalpath = RetracePath(startnode, targetnode);
                return finalpath;
            }

            foreach (Node neighbor in Grids.Instance.GetNeighbor(currentnode))
            {
                if (!neighbor.Walkable || discountnode.Contains(neighbor))
                {
                    continue;
                }

                int currenttoneighbor = currentnode.GCost + GetDistance(currentnode, neighbor);
                if (currenttoneighbor < neighbor.GCost || !countnode.Contains(neighbor))
                {
                    neighbor.GCost = currenttoneighbor;
                    neighbor.HCost = GetDistance(neighbor, targetnode);
                    neighbor.ParentNode = currentnode;

                    if (!countnode.Contains(neighbor))
                    {
                        countnode.Add(neighbor);
                    }
                }
            }
        }
        return new List<Node>();
    }

    /// <summary>
    /// Starts this instance.
    /// </summary>
    void Awake()
    {
        mInstance = this;
    }

    /// <summary>
    /// 計算完A*沿著Parent找回初始Node
    /// </summary>
    /// <param name="starNode">初始node</param>
    /// <param name="endNode">終點node</param>
    List<Node> RetracePath(Node starNode, Node endNode)
    {
        List<Node> path = new List<Node>();
        Node currentNode = endNode;

        while (currentNode != starNode)
        {
            path.Add(currentNode);
            currentNode = currentNode.ParentNode;
        }
        path.Reverse();
        return path;
    }

    /// <summary>
    /// 兩個Node之間的距離
    /// </summary>
    /// <param name="nodeA">第一個Node</param>
    /// <param name="nodeB">第二個Node</param>
    /// <returns>距離</returns>
    int GetDistance(Node nodeA, Node nodeB)
    {
        int costx = Mathf.Abs(nodeA.Numberx - nodeB.Numberx);
        int costy = Mathf.Abs(nodeA.Numbery - nodeB.Numbery);
        if (costy < costx)
        {
            return (costx - costy) * STRAIGHT_COST + costy * SLASH_COST;
        }
        else
        {
            return (costy - costx) * STRAIGHT_COST + costx * SLASH_COST;
        }
    }
}
