﻿using UnityEngine;

/// <summary>
/// 搖桿移動
/// </summary>
class JoyStickMove : IMoveType
{
    Vector3 mTargetPos;
    float mMoveSpeed;

    public JoyStickMove(float moveSpeed)
    {
        mMoveSpeed = moveSpeed;
        if (VirtualJoyStick.Instance == null)
        {
            Debug.LogError("No VirtualJoyStick");
        }
    }

    public void Move(Transform moveObject)
    {
        mTargetPos = moveObject.transform.position + VirtualJoyStick.Instance.JoyStickDirection;
        if (!Grids.Instance.NodeFromWorldPoint(mTargetPos).Walkable)
        {
            return;
        }
        moveObject.transform.position += Vector3.Normalize(mTargetPos - moveObject.transform.position) * Time.deltaTime * mMoveSpeed;
        moveObject.transform.rotation = Quaternion.LookRotation(new Vector3(VirtualJoyStick.Instance.JoyStickDirection.x, 0, VirtualJoyStick.Instance.JoyStickDirection.z));
    }
}

