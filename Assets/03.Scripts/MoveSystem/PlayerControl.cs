﻿using UnityEngine;

public class PlayerControl : CharacterControl
{
    [HideInInspector] public Vector3 MoveDistance;
    [HideInInspector] public bool JoyStickIsMove;

    public static PlayerControl Instance
    {
        get
        {
            if (mInstance == null)
            {
                var gamemanager = new GameObject("Hero");
                gamemanager.AddComponent<PlayerControl>();
            }
            return mInstance;
        }
    }

    static PlayerControl mInstance;
    IMoveType mJoyStickMove;
    IMoveType mMoveByClickDetect;
    [SerializeField] Animator mAnim;
    GameObject mPlayer;
    bool mPathNotClear = true;

    /// <summary>
    /// 移動動畫,並接收JoyStick移動訊息
    /// </summary>
    public void FixedUpdate()
    {
        mAnim.SetBool("Move", IsMoving);
        Move();
    }

    /// <summary>
    /// 移動方式
    /// </summary>
    public override void Move()
    {
        JoyStickMove();
        MoveByClickDetect();
    }

    /// <summary>
    /// 移動並藉A*產生的Gird判定可不可走
    /// </summary>
    public void JoyStickMove()
    {
        if (JoyStickIsMove)
        {
            IsMoving = true;
            mJoyStickMove.Move(mPlayer.transform);
        }
    }

    /// <summary>
    /// 點擊移動，若移動中換成Joystick移動則清空A*的List
    /// </summary>
    public void MoveByClickDetect()
    {
        if (JoyStickIsMove && mPathNotClear)
        {
            mMoveByClickDetect.Move(mPlayer.transform);
            mPathNotClear = false;
        }
        else if (!JoyStickIsMove)
        {
            mMoveByClickDetect.Move(mPlayer.transform);
            mPathNotClear = true;
        }
    }

    void Awake()
    {
        mInstance = this;
    }

    /// </summary>
    /// 找Player的Animator 找Grid判定是否可走，並New出Interface
    /// </summary>
    void Start()
    {
        mJoyStickMove = new JoyStickMove(MoveSpeed);
        mMoveByClickDetect = new MoveByClickDetect(MoveSpeed, this);
        mPlayer = ObjectPoolManager.Instance.GetObjectInGame("Hero");
    }
}
