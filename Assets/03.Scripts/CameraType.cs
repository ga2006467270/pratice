﻿using UnityEngine;

public interface ICameraType
{
    float CameraSpeed
    {
        get;
        set;
    }

    void CameraMove(GameObject cameraObject, GameObject camera);
    void SetCameraSpeed(float speed);
}

class KeyBoardCamera : ICameraType
{
    public float CameraSpeed
    {
        get
        {
            return mCameraSpeed;
        }

        set
        {
            mCameraSpeed = value;
        }
    }

    float mCameraSpeed;

    public void CameraMove(GameObject cameraObject, GameObject camera)
    {
        if (Input.GetKey(KeyCode.A))
        {
            cameraObject.transform.position -= cameraObject.transform.right * CameraSpeed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.W))
        {
            cameraObject.transform.position += cameraObject.transform.forward * CameraSpeed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.D))
        {
            cameraObject.transform.position += cameraObject.transform.right * CameraSpeed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.S))
        {
            cameraObject.transform.position -= cameraObject.transform.forward * CameraSpeed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.E))
        {
            cameraObject.transform.Rotate(0, 5, 0, Space.World);
        }
        if (Input.GetKey(KeyCode.Q))
        {
            cameraObject.transform.Rotate(0, -5, 0, Space.World);
        }
        if (Input.GetKey(KeyCode.KeypadMinus))
        {
            cameraObject.transform.position += camera.transform.forward * CameraSpeed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.KeypadPlus))
        {
            cameraObject.transform.position -= camera.transform.forward * CameraSpeed * Time.deltaTime;
        }
    }

    public void SetCameraSpeed(float speed)
    {
        mCameraSpeed = speed;
    }
}

class MouseMoveCamera : ICameraType
{
    const float SCREEN_DETECT_OFFSET = 0.98f;

    public float CameraSpeed
    {
        get
        {
            return mCameraSpeed;
        }

        set
        {
            mCameraSpeed = value;
        }
    }

    float mCameraSpeed;
    float mScreenWidth;
    float mScreenHeight;

    public MouseMoveCamera()
    {
        mScreenWidth = Screen.width;
        mScreenHeight = Screen.height;
    }

    public void CameraMove(GameObject cameraObject, GameObject camera)
    {
        if (Input.mousePosition.x > mScreenWidth * SCREEN_DETECT_OFFSET)
        {
            cameraObject.transform.position += new Vector3(mCameraSpeed * Time.deltaTime, 0, 0);
        }
        if (Input.mousePosition.x < mScreenWidth * (1 - SCREEN_DETECT_OFFSET))
        {
            cameraObject.transform.position -= new Vector3(mCameraSpeed * Time.deltaTime, 0, 0);
        }
        if (Input.mousePosition.y > mScreenHeight * SCREEN_DETECT_OFFSET)
        {
            cameraObject.transform.position += new Vector3(0, 0, mCameraSpeed * Time.deltaTime);
        }
        if (Input.mousePosition.y < mScreenHeight * (1 - SCREEN_DETECT_OFFSET))
        {
            cameraObject.transform.position -= new Vector3(0, 0, mCameraSpeed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.KeypadMinus))
        {
            cameraObject.transform.position += camera.transform.forward * CameraSpeed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.KeypadPlus))
        {
            cameraObject.transform.position -= camera.transform.forward * CameraSpeed * Time.deltaTime;
        }
    }

    public void SetCameraSpeed(float speed)
    {
        CameraSpeed = speed;
    }
}

class FollowCamera : ICameraType
{
    public float CameraSpeed
    {
        get
        {
            return mCameraSpeed;
        }

        set
        {
            mCameraSpeed = value;
        }
    }

    float mCameraSpeed;
    GameObject mTarget;
    Vector3 mOffset;

    public FollowCamera()
    {
        mTarget = ObjectPoolManager.Instance.GetObjectInGame("Hero");
        mOffset = Camera.main.transform.position - mTarget.transform.position;
    }

    public void CameraMove(GameObject cameraObject, GameObject camera)
    {
        if (mOffset == Vector3.zero)
        {
            mOffset = camera.transform.position - mTarget.transform.position;
        }
        cameraObject.transform.position = mTarget.transform.position + mOffset;

        if (Input.GetKey(KeyCode.KeypadMinus))
        {
            cameraObject.transform.position += camera.transform.forward * CameraSpeed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.KeypadPlus))
        {
            cameraObject.transform.position -= camera.transform.forward * CameraSpeed * Time.deltaTime;
        }
    }

    public void SetCameraSpeed(float speed)
    {
        CameraSpeed = speed;
    }
}

class StaticCamera : ICameraType
{
    public float CameraSpeed
    {
        get
        {
            throw new System.NotImplementedException();
        }

        set
        {
        }
    }

    public void CameraMove(GameObject cameraObject, GameObject camera)
    {
    }

    public void SetCameraSpeed(float speed)
    {
    }
}

