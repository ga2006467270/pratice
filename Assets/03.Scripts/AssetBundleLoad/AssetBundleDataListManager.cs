﻿using System.IO;
using System;
using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.Networking;

public class AssetBundleDataListManager : ISaveLoadInterface
{
    /// <summary>
    /// 將AssetBundleData儲存上傳到HTTP
    /// </summary>
    public void Save(string path, string name)
    {
        SaveJsonFile(path, name, AssetBundleManager.Instance.Version, AssetLoadURL.GetAssetLoadSystemURL(AssetLoadURL.eBundleSaveVersion.AssetBundleSystem));
    }

    public void Load(string path)
    {
        //AssetBundleManager.Instance.StartCoroutine(DownloadJsonData(path));
        LoadAssetBundleData();
    }

    /// <summary>
    /// 在BundleData中找Json資料，可能要由Jenkins產出，因此不可自選讀取路徑
    /// </summary>
    public static void SaveJsonFile(string jsonPath, string name, AssetBundleManager.eVersion version, AssetLoadURL.sBundleStruct[] url)
    {
        AssetBundleData json;
        string loadpath = URLManager.GetProjectFolderPath() + "/BundleData";
        using (StreamWriter stream = new StreamWriter(URLManager.GetJsonPath(jsonPath, name, SaveFileFormat.SaveFromat(eSaveFileFormat.json))))
        {
            json = new AssetBundleData(loadpath, version, url);
            string jsonstring = JsonUtility.ToJson(json);
            stream.Write(jsonstring);
        }
    }

    void LoadAssetBundleData()
    {
    }

    IEnumerator DownloadJsonData(string path)
    {
        using (var request = UnityWebRequest.Get(path))
        {
            yield return request.SendWebRequest();
            AssetBundleData jsondata = JsonUtility.FromJson<AssetBundleData>(request.downloadHandler.text);
            foreach (AssetBundleData.sBundleAsset bundleasset in jsondata.JsonBundleAsset)
            {
                AssetBundleManager.Instance.SetBundleData(URLManager.SetAssetBundle(AssetBundleManager.Instance.PlayModePlatform, bundleasset.Version, bundleasset.AssetBundle), bundleasset.ObjectPoolType, bundleasset.Version);
            }
        }
    }
}

[Serializable]
public class AssetBundleData
{
    [Serializable]
    public struct sBundleAsset
    {
        public string AssetBundle;
        public ObjectPoolManager.eObjectPoolType ObjectPoolType;
        public AssetBundleManager.eVersion Version;

        public sBundleAsset(string assetBundle, ObjectPoolManager.eObjectPoolType objectPoolType, AssetBundleManager.eVersion version)
        {
            AssetBundle = assetBundle;
            ObjectPoolType = objectPoolType;
            Version = version;
        }
    }

    public List<sBundleAsset> JsonBundleAsset
    {
        get
        {
            return mJsonBundleAsset;
        }
    }

    [SerializeField]
    List<sBundleAsset> mJsonBundleAsset = new List<sBundleAsset>();

    public AssetBundleData(string loadPath, AssetBundleManager.eVersion version, AssetLoadURL.sBundleStruct[] url)
    {
        for (int bundlelength = 0; bundlelength < url.Length; bundlelength++)
        {
            mJsonBundleAsset.Add(LoadAssetBundleVersion(url[bundlelength], version));
        }
    }

    sBundleAsset LoadAssetBundleVersion(AssetLoadURL.sBundleStruct path, AssetBundleManager.eVersion version)
    {
        return new sBundleAsset(path.BundleName, path.PoolType, version);
    }
}

