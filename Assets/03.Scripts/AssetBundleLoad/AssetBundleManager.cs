﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

public class AssetBundleManager : MonoBehaviour
{
    public const int ASSET_LENGTH = 7;
    public const string ASSET_FILE_NAME = "sAssetBundle";

    public enum eSystemType
    {
        LoadAssetBundle,
    }

    public enum ePlatform
    {
        Editor,
        Andriod,
    }

    public enum eVersion
    {
        Normal,
        Chrismas,
    }

    public static AssetBundleManager Instance
    {
        get
        {
            if (mInstance == null)
            {
                var gamemanager = new GameObject("AssetBundleManager");
                gamemanager.AddComponent<AssetBundleManager>();
            }
            return mInstance;
        }
    }

    public ObjectPoolManager.eObjectPoolType ObjectPoolType
    {
        get
        {
            return mObjectPoolType;
        }

        set
        {
            mObjectPoolType = value;
        }
    }

    public ePlatform SavePlatform
    {
        get
        {
            return mSavePlatform;
        }

        set
        {
            mSavePlatform = value;
        }
    }

    public bool CanLoadAsset
    {
        get
        {
            return mCanLoadAsset;
        }

        set
        {
            mCanLoadAsset = value;
        }
    }

    public eVersion Version
    {
        get
        {
            return mVersion;
        }

        set
        {
            mVersion = value;
        }
    }

    public ePlatform PlayModePlatform
    {
        get
        {
            switch (Application.platform)
            {
                case RuntimePlatform.Android:
                    return ePlatform.Andriod;
                case RuntimePlatform.WindowsEditor:
                    return ePlatform.Editor;
                default:
                    Debug.LogError("版本號有誤");
                    return ePlatform.Editor;
            }
        }
    }

    public bool LoadAssetDone
    {
        get
        {
            return mLoadAssetDone;
        }

        set
        {
            mLoadAssetDone = value;
        }
    }

    public bool CanChangeScene = false;

    Cache mAssetCache;
    bool mCanLoadAsset = false;
    bool mLoadAssetDone = false;
    [SerializeField] ePlatform mSavePlatform;
    [SerializeField] eVersion mVersion;
    ObjectPoolManager.eObjectPoolType mObjectPoolType;
    static AssetBundleManager mInstance;
    List<string> mIsDoneAssetBundle = new List<string>();
    AssetBundleStore mAssetStore;

    /// <summary>
    /// 讀取BundleData內的物品，如果有特定assetName則只讀assetName，若無則全讀
    /// </summary>
    public void SetBundleData(string bundleURL, ObjectPoolManager.eObjectPoolType poolTypeInString, eVersion version)
    {
        if (mIsDoneAssetBundle.Contains(bundleURL))
        {
            Debug.LogWarning("你已經載入過這個Bundle了");
            return;
        }
        else
        {
            mIsDoneAssetBundle.Add(bundleURL);
        }
        LoadModel(bundleURL, poolTypeInString, version);
        StartCoroutine(DownloadModel(bundleURL, poolTypeInString, version));
    }

    public void LoadAssetBundle()
    {
        mAssetStore = new AssetBundleStore(new SimpleAssetBundleFactory());
        mAssetStore.AssetOrder(eSystemType.LoadAssetBundle);
    }

    public void SaveAssetBundle()
    {
#if UNITY_EDITOR
        string savepath = UnityEditor.EditorUtility.OpenFolderPanel("存檔路徑", "", "");
        var save = new SaveLoad(SaveLoad.eSystemType.AssetBundle);
        save.Save(savepath, ASSET_FILE_NAME);
#endif
    }

    /// <summary>
    /// LoadAssetBundle，如果沒有特定AssetName則Load全部物件，並根據eObjectPoolType來判定要進入哪個物件池
    /// </summary>
    /// <param name="bundleURL">網址</param>
    /// <param name="assetsName">載入資料</param>
    /// <param name="poolType">哪個物件池</param>
    /// <returns></returns>
    IEnumerator DownloadModel(string bundleURL, ObjectPoolManager.eObjectPoolType poolType, eVersion version)
    {
        using (UnityWebRequest request = UnityWebRequestAssetBundle.GetAssetBundle(bundleURL, (uint)version, 0))
        {
            yield return request.SendWebRequest();
            AssetBundle assetbundle = (request.downloadHandler as DownloadHandlerAssetBundle).assetBundle;
            var objects = assetbundle.LoadAllAssets();
            for (int objectlength = 0; objectlength < objects.Length; objectlength++)
            {
                AddsToPool(objects[objectlength], poolType);
            }
            if (request.isDone)
            {
                if (LoadAssetBundleUI.Instance.mLoadingText != null)
                {
                    LoadAssetBundleUI.Instance.mLoadingText.text = "讀取完成";
                    LoadAssetBundleUI.Instance.SetProgressSlider(AssetLoadURL.GetAssetLoadSystemURL(AssetLoadURL.eBundleSaveVersion.AssetBundleSystem).Length);
                }
                ObjectPoolManager.Instance.ReSetCharacter();
                AssetBundleManager.Instance.LoadAssetDone = true;
            }
        }
    }

    void LoadModel(string path, ObjectPoolManager.eObjectPoolType poolType, eVersion version)
    {
        UnityWebRequest request = new UnityWebRequest(path);
        AssetBundle assetbundle = (request.downloadHandler as DownloadHandlerAssetBundle).assetBundle;
        var objects = assetbundle.LoadAllAssets();
        for (int objectlength = 0; objectlength < objects.Length; objectlength++)
        {
            AddsToPool(objects[objectlength], poolType);
        }
        if (request.isDone)
        {
            if (LoadAssetBundleUI.Instance.mLoadingText != null)
            {
                LoadAssetBundleUI.Instance.mLoadingText.text = "讀取完成";
                LoadAssetBundleUI.Instance.SetProgressSlider(AssetLoadURL.GetAssetLoadSystemURL(AssetLoadURL.eBundleSaveVersion.AssetBundleSystem).Length);
            }
            ObjectPoolManager.Instance.ReSetCharacter();
            AssetBundleManager.Instance.LoadAssetDone = true;
        }
    }

    void Awake()
    {
        mInstance = this;
        SetCache();
    }

    void Start()
    {
#if UNITY_EDITOR
        AssetBundleManager.Instance.LoadAssetBundle();
#endif
    }

    void SetCache()
    {
#if UNITY_EDITOR
        string cachepath = Application.temporaryCachePath;
        if (!Directory.Exists(cachepath))
        {
            Directory.CreateDirectory(cachepath);
            Debug.Log("創造cache資料夾");
        }
        if (mAssetCache.valid)
        {
            mAssetCache = Caching.AddCache(cachepath);
            Caching.currentCacheForWriting = mAssetCache;
        }
        Debug.Log("設定Cache完成");
#endif
    }

    /// <summary>
    /// 根據PoolType將各個Object放進物件池內，也在此轉換Object型態
    /// </summary>
    /// <param name="objects"></param>
    /// <param name="poolType"></param>
    void AddsToPool(Object addToPoolObject, ObjectPoolManager.eObjectPoolType poolType)
    {
        switch (poolType)
        {
            case ObjectPoolManager.eObjectPoolType.ObjectPool:
                ObjectPoolManager.Instance.AddObject(addToPoolObject as GameObject, addToPoolObject.name);
                break;
            case ObjectPoolManager.eObjectPoolType.ChracterPool:
                ObjectPoolManager.Instance.AddCharacter(addToPoolObject as GameObject);
                break;
            case ObjectPoolManager.eObjectPoolType.TexturePool:
                ObjectPoolManager.Instance.AddText(addToPoolObject as Texture2D, addToPoolObject.name);
                break;
            case ObjectPoolManager.eObjectPoolType.MaterialPool:
                ObjectPoolManager.Instance.AddMaterial(addToPoolObject as Material, addToPoolObject.name);
                break;
        }
    }

    public void ChangeSceneToInteract()
    {
        StartCoroutine(ChangeScene(ObjectPoolManager.eSystemType.NPCInteract));
    }

    IEnumerator ChangeScene(ObjectPoolManager.eSystemType systemtype)
    {
        if (!CanChangeScene)
        {
            Debug.Log("等待載入");
            yield return new WaitForSeconds(0.1f);
            StartCoroutine(ChangeScene(ObjectPoolManager.eSystemType.NPCInteract));
        }
        else
        {
            Debug.Log("載入");
            ObjectPoolManager.Instance.LoadObjectPoolToGame(ObjectPoolManager.eSystemType.NPCInteract);
            CameraController.Instance.ChangeCameraType(CameraController.eSystemType.Follow);
            Grids.Instance.ResetNode();
        }
        yield return null;
    }
}

public class AssetBundleStore
{
    SimpleAssetBundleFactory mFactory;

    public AssetBundleStore(SimpleAssetBundleFactory factory)
    {
        mFactory = factory;
    }

    public void AssetOrder(AssetBundleManager.eSystemType assetEnum)
    {
        IAssetLoadType assetloadtype;
        assetloadtype = mFactory.CreatAsset(assetEnum);
        assetloadtype.LoadAssetBundle();
    }
}

public class SimpleAssetBundleFactory
{
    public IAssetLoadType CreatAsset(AssetBundleManager.eSystemType assetEnum)
    {
        switch (assetEnum)
        {
            case AssetBundleManager.eSystemType.LoadAssetBundle:
                return new AssetLoad();
        }
        return null;
    }
}

public class URLManager
{
    const string HTTP_URL = "http://192.168.37.36/";

    public static string GetURL(AssetBundleManager.ePlatform platform, string fileName, AssetBundleManager.eVersion version, string fileType)
    {
        return HTTP_URL + platform + "/" + version + "/" + fileName + fileType;
    }

    public static string GetURL(AssetBundleManager.ePlatform platform, string fileName, string fileType)
    {
        return HTTP_URL + platform + "/" + fileName + fileType;
    }

    public static string GetURL(AssetBundleManager.ePlatform platform)
    {
        return HTTP_URL + platform + "/";
    }

    public static string GetJsonPath(string jsonPath, string fileName, string fileType)
    {
        return jsonPath + "/" + fileName + fileType;
    }

    public static string SetAssetBundle(AssetBundleManager.ePlatform platform, AssetBundleManager.eVersion version, string assetbundleName)
    {
        return HTTP_URL + platform + "/" + version + "/" + assetbundleName;
    }

    public static string GetProjectFolderPath()
    {
        string datapath = Application.dataPath;
        string loadpath = datapath.Remove(Application.dataPath.Length - AssetBundleManager.ASSET_LENGTH) + "/";
        return loadpath;
    }
}
