﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// UI 管理進度條
/// </summary>
public class LoadAssetBundleUI : MonoBehaviour
{
    public static LoadAssetBundleUI Instance
    {
        get
        {
            if (mInstance == null)
            {
                var gamemanager = new GameObject("LoadAssetBundleUI");
                gamemanager.AddComponent<LoadAssetBundleUI>();
            }
            return mInstance;
        }
        set
        {
            mInstance = value;
        }
    }

    public Text mLoadingText;

    static LoadAssetBundleUI mInstance;
    [SerializeField] Slider mProgressSlider;

    public void ChangeVersion()
    {
        if ((int)AssetBundleManager.Instance.Version == System.Enum.GetValues(typeof(AssetBundleManager.eVersion)).Length - 1)
        {
            AssetBundleManager.Instance.Version = 0;
        }
        else
        {
            AssetBundleManager.Instance.Version = AssetBundleManager.Instance.Version + 1;
        }
    }

    public void LoadNPCInteractButton()
    {
        if (!AssetBundleManager.Instance.CanLoadAsset)
        {
            AssetBundleManager.Instance.LoadAssetBundle();
        }
        else
        {
            LoadInteractScene();
        }
    }

    /// <summary>
    /// 重新設定版本 清除占存的Cache
    /// </summary>
    public void ResetVersion()
    {
        Caching.ClearCache();
        AssetBundleManager.Instance.LoadAssetBundle();
    }

    public void SetProgressSlider(int loadLength)
    {
        mProgressSlider.value += 1f / loadLength;
        if (mProgressSlider.value >= 1f)
        {
            mLoadingText.text = "讀取完成...";
            LoadInteractScene();
            Caching.ClearCache();
        }
    }

    public void SaveDataToBinary()
    {
        AssetBundleManager.Instance.SaveAssetBundle();
    }

    void Awake()
    {
        mInstance = this;
    }

    void LoadInteractScene()
    {
        AssetBundleManager.Instance.ChangeSceneToInteract();
        LoadInteractNPC();
    }

    public void LoadInteractNPC()
    {
        mLoadingText.text = "加載中...";
        string npcpath;
        npcpath = URLManager.GetURL(AssetBundleManager.Instance.PlayModePlatform, "NPCdb", AssetBundleManager.Instance.Version, SaveFileFormat.SaveFromat(eSaveFileFormat.txt));
        AssetBundleManager.Instance.CanLoadAsset = true;
        var load = new SaveLoad(SaveLoad.eSystemType.NPCInterct);
        load.Load(npcpath);
    }
}
