﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAnimatorController : MonoBehaviour
{
    [SerializeField]Animator anim;
    void Awake()
    {
        ObjectPoolManager.Instance.AddAnimator(anim);
    }

    public void AttackDone()
    {
        anim.SetBool("Attack", false);
    }

    /// <summary>
    /// 結束攻擊時回傳值讓回合繼續進行
    /// </summary>
    public void EndAttackAnim()
    {
        BattleManager.Instance.IsInAnimation = false;
    }
}
