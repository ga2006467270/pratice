﻿using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System.Collections;

public class BattleManager : MonoBehaviour
{
    public const int MAX_CHARACTER = 6;
    public static BattleManager Instance
    {
        get
        {
            if (mInstance == null)
            {
                var gamemanager = new GameObject("BattleManager");
                gamemanager.AddComponent<BattleManager>();
            }
            return mInstance;
        }
    }

    public BattleNode[] AllyNode
    {
        get
        {
            return mAllyNode;
        }

        set
        {
            mAllyNode = value;
        }
    }

    public BattleNode[] EnemyNode
    {
        get
        {
            return mEnemyNode;
        }

        set
        {
            mEnemyNode = value;
        }
    }

    public bool IsInAnimation
    {
        get
        {
            return mIsInAnimation;
        }

        set
        {
            mIsInAnimation = value;
        }
    }

    static BattleManager mInstance;
    Vector3 mOriginalCameraPos;
    Quaternion mOriginalCameraRotate;
    [SerializeField] BattleNode[] mAllyNode = new BattleNode[MAX_CHARACTER];
    [SerializeField] BattleNode[] mEnemyNode = new BattleNode[MAX_CHARACTER];
    bool mIsInBattle;
    bool mIsInAnimation;

    /// <summary>
    /// 一個一個按照順序攻擊， 敵我雙方皆要
    /// </summary>
    public void Attack()
    {
        if (mIsInBattle == false)
        {
            Debug.Log("攻擊");
            mIsInBattle = true;
            NextCharacterTurn();
        }
        else
        {
            Debug.LogWarning("請在戰鬥結束後再按攻擊");
        }
    }

    public void NextCharacterTurn()
    {
        for (int nodecount = 0; nodecount < mAllyNode.Length; nodecount++)
        {
            if (mAllyNode[nodecount].Down == false)
            {
                if (mAllyNode[nodecount].WasTurnPrev == false)
                {
                    mAllyNode[nodecount].IsTurn = true;
                    StartCoroutine(mAllyNode[nodecount].DoTurn());
                    Debug.Log(nodecount + " 號進行攻擊");
                    return;
                }
                else if ((nodecount == (mAllyNode.Length - 1)) && mAllyNode[nodecount].WasTurnPrev == true)
                {
                    Debug.LogWarning("換敵人攻擊");
                }
            }
        }

        for (int nodecount = 0; nodecount < mEnemyNode.Length; nodecount++)
        {
            if (mEnemyNode[nodecount].Down == false)
            {
                if (mEnemyNode[nodecount].WasTurnPrev == false)
                {
                    mEnemyNode[nodecount].IsTurn = true;
                    StartCoroutine(mEnemyNode[nodecount].DoTurn());
                    Debug.Log(nodecount + " 號敵人進行攻擊");
                    return;
                }
                else if ((nodecount == (mEnemyNode.Length - 1)) && mEnemyNode[nodecount].WasTurnPrev)
                {
                    Debug.LogWarning("戰鬥結束，重製所有戰鬥玩家怪物狀態");
                    ResetCharacterTurn();
                    return;
                }
            }
            if (nodecount == mEnemyNode.Length - 1)
            {
                Debug.Log("敵人全數死亡");
                StartCoroutine(LeaveTheBattle());
            }
        }

    }

    void ResetCharacterTurn()
    {
        for (int nodecount = 0; nodecount < mAllyNode.Length; nodecount++)
        {
            mAllyNode[nodecount].ResetTurn();
        }

        for (int nodecount = 0; nodecount < mEnemyNode.Length; nodecount++)
        {
            mEnemyNode[nodecount].ResetTurn();
        }
        mIsInBattle = false;
    }

    /// <summary>
    /// 1.進入戰鬥時，輸入怪物名稱，在MonsterDic中讀取 怪物預設資料 並放置到EnemyField上
    /// 2.放置Ally到場上並排序
    /// 3.放置UI，關閉主介面以外的UI(目前只放攻擊指令，也沒有主介面) PS:選擇攻擊指令時螢幕變暗
    /// </summary>
    /// <param name="enemyStats"></param>
    public void Battle(BattleNode[] enemyStats)
    {
        Debug.Log("進入戰鬥");
        BattleUI.Instance.ChangeBattleSmooth();
        ChangeToBattleField();
        LoadAlly(mAllyNode);
        LoadBattleUI();
        SetEnemy(enemyStats);
    }

    /// <summary>
    /// 設定接下來準備戰鬥的enemy的Stats
    /// </summary>
    /// <param name="monsterStats"></param>
    public void SetEnemyStats(CharacterStats[] monsterStats)
    {
        for (int monsterLength = 0; monsterLength < monsterStats.Length; monsterLength++)
        {
            mEnemyNode[monsterLength].NodeCharacterStats = new CharacterStats(monsterStats[monsterLength].AttackValue, monsterStats[monsterLength].DefendValue, monsterStats[monsterLength].CurrentHp, monsterStats[monsterLength].CharacterNumber, mEnemyNode[monsterLength], monsterStats[monsterLength].Name);
            Debug.Log(monsterStats[monsterLength].CharacterNumber);
        }
    }

    /// <summary>
    /// 開/關Battle相關的Object
    /// </summary>
    public void ChangeBattleObject()
    {
        ObjectPoolManager.Instance.GetObjectInGame("EnemyField").SetActive(!ObjectPoolManager.Instance.GetObjectInGame("EnemyField").activeSelf);
        ObjectPoolManager.Instance.GetObjectInGame("AllyField").SetActive(!ObjectPoolManager.Instance.GetObjectInGame("AllyField").activeSelf);
        ObjectPoolManager.Instance.GetObjectInGame("BattleField").SetActive(!ObjectPoolManager.Instance.GetObjectInGame("BattleField").activeSelf);
    }

    /// <summary>
    /// 回傳一個怪物，讓別人去打它，如果沒有怪物了，就代表戰鬥勝利
    /// </summary>
    /// <param name="group"></param>
    /// <returns></returns>
    // 待修正 敵我雙方共用同一個Stats
    public BattleNode GetTarget(int group)
    {
        switch (group)
        {
            case 0:
                for (int enemynodecount = 0; enemynodecount < mEnemyNode.Length; enemynodecount++)
                {
                    if (mEnemyNode[enemynodecount].Down == false)
                    {
                        return mEnemyNode[enemynodecount];
                    }
                }
                Debug.LogError("沒有敵人，戰鬥結束");
                break;
            case 1:
                for (int allynodecount = 0; allynodecount < mAllyNode.Length; allynodecount++)
                {
                    if (mAllyNode[allynodecount].Down == false)
                    {
                        return mAllyNode[allynodecount];
                    }
                }
                Debug.LogError("沒有自己人，戰鬥結束");
                break;
            default:
                Debug.LogError("錯誤的 Group : " + group);
                break;
        }
        return null;
    }

    /// <summary>
    /// 離開戰鬥，將Camera回到原來的位置，清空戰鬥平台，關閉Battle相關UI
    /// </summary>
    public IEnumerator LeaveTheBattle()
    {
        Debug.Log("戰鬥結束，1秒結算後跳回玩家");
        yield return new WaitForSeconds(1f);
        CameraController.Instance.SetCamera(mOriginalCameraPos, mOriginalCameraRotate);
        CameraController.Instance.ChangeCameraType(CameraController.eSystemType.KeyBoard);
        ChangeBattleObject();
        for (int enemynodelength = 0; enemynodelength < mEnemyNode.Length; enemynodelength++)
        {
            Destroy(mEnemyNode[enemynodelength].Character);
            mEnemyNode[enemynodelength].ResetNode();
        }
        for (int allynodelength = 0; allynodelength < mAllyNode.Length; allynodelength++)
        {
            Destroy(mAllyNode[allynodelength].Character);
            mAllyNode[allynodelength].ResetNode();
        }
        mIsInBattle = false;
        OpenUI();
        BattleUI.Instance.CloseBattleUI();
        ResetCharacterTurn();
    }

    /// <summary>
    /// 戰鬥開始時設定人物資訊，目前設定新的戰鬥都會重製血量，不會根據上一場戰鬥扣多少血而繼承
    /// </summary>
    /// <param name="playersStats"></param>
    public void SetPlayerStats(MobStats.sMonsterStats[] playersStats)
    {
        for (int statslength = 0; statslength < playersStats.Length; statslength++)
        {
            CharacterStats character = new CharacterStats(playersStats[statslength].Attack, playersStats[statslength].Defend, playersStats[statslength].MaxHp, playersStats[statslength].MonsterNumber, mAllyNode[statslength], playersStats[statslength].MonsterName);
            if (mAllyNode[statslength].Character != null)
            {
                Debug.Log("已有角色");
                continue;
            }
            mAllyNode[statslength].NodeCharacterStats = character;
        }
    }

    void Awake()
    {
        if (mInstance != null)
        {
            Destroy(this);
        }
        else
        {
            mInstance = this;
        }
        // 隱藏由HTTP讀取模式
        //loadmonsterdata.Load(URLManager.GetURL(AssetBundleManager.ePlatform.Editor, "Monsterdb", AssetBundleManager.eVersion.Normal, SaveFileFormat.SaveFromat(eSaveFileFormat.json)));
    }

    void Start()
    {
        SaveLoad loadmonsterdata = new SaveLoad(SaveLoad.eSystemType.Battle);
        loadmonsterdata.Load(URLManager.GetProjectFolderPath() + "BattleData/Monsterdb" + SaveFileFormat.SaveFromat(eSaveFileFormat.json));
        // 隱藏由HTTP讀取模式
        BattleStatsSave battleStatsSave = new BattleStatsSave();
        //battleStatsSave.LoadPlayerStats(URLManager.GetURL(AssetBundleManager.ePlatform.Editor, "Playerdb", SaveFileFormat.SaveFromat(eSaveFileFormat.json)));
        battleStatsSave.LoadPlayerStats(URLManager.GetProjectFolderPath() + "BattleData/Playerdb" + SaveFileFormat.SaveFromat(eSaveFileFormat.json));
    }

    /// <summary>
    /// 遊戲開始時將node放入BattleManager中方便控制
    /// </summary>
    /// <param name="node"></param>
    /// <param name="nodeName"></param>
    public void AddToBattleNodeToArray(BattleNode node, string nodeName)
    {
        switch (nodeName)
        {
            case "AllyNode":
                for (int nodelength = 0; nodelength < AllyNode.Length; nodelength++)
                {
                    if (AllyNode[nodelength] == null)
                    {
                        mAllyNode[nodelength] = node;
                        return;
                    }
                }
                break;
            case "EnemyNode":
                for (int nodelength = 0; nodelength < mEnemyNode.Length; nodelength++)
                {
                    if (mEnemyNode[nodelength] == null)
                    {
                        mEnemyNode[nodelength] = node;
                        return;
                    }
                }
                break;
            default:
                Debug.Log("錯誤的Node名稱");
                return;
        }
    }

    /// <summary>
    /// 將攝影機轉換至戰鬥場地Active戰鬥場地，移動Camera位置，並關閉UI
    /// </summary>
    void ChangeToBattleField()
    {
        ChangeBattleObject();
        CloseUI();
        CameraController.Instance.ChangeCameraType(CameraController.eSystemType.None);
        mOriginalCameraPos = CameraController.Instance.gameObject.transform.position;
        mOriginalCameraRotate = CameraController.Instance.gameObject.transform.rotation;
        Debug.Log(mOriginalCameraPos);
        Debug.Log(mOriginalCameraRotate);
        CameraController.Instance.SetCamera(new Vector3(1203, 8.5f, -1200), Quaternion.Euler(4, -76, 0));
    }

    /// <summary>
    /// 戰鬥時將主介面UI以外的UI皆隱藏
    /// </summary>
    void CloseUI()
    {
        ObjectPoolManager.Instance.GetObjectInGame("JoyStickCanvas").SetActive(false);
    }

    /// <summary>
    /// 將原本的UI開啟
    /// </summary>
    void OpenUI()
    {
        ObjectPoolManager.Instance.GetObjectInGame("JoyStickCanvas").SetActive(true);
    }

    /// <summary>
    /// 開始戰鬥時設定敵人 傳入該敵人的list
    /// </summary>
    /// <param name="enemyArray"></param>
    void SetEnemy(BattleNode[] enemyStats)
    {
        for (int enemyobject = 0; enemyobject < enemyStats.Length; enemyobject++)
        {
            if (mEnemyNode[enemyobject].Character != null)
            {
                Debug.Log("已有角色" + mEnemyNode[enemyobject].Character.name);
                continue;
            }
            // GameObject enemy = Instantiate(ObjectPoolManager.Instance.GetCharacter(enemyStats[enemyobject].CharacterNumber), mEnemyNode[enemyobject].transform.position, mEnemyNode[enemyobject].transform.rotation);
            // 預設敵人 1 號
            Debug.Log(mEnemyNode[enemyobject].NodeCharacterStats.CharacterNumber);
            GameObject enemy = Instantiate(ObjectPoolManager.Instance.GetCharacter(mEnemyNode[enemyobject].NodeCharacterStats.CharacterNumber), mEnemyNode[enemyobject].transform.position, mEnemyNode[enemyobject].transform.rotation);
            enemy.name = "name" + enemyobject;
            Animator anim = ObjectPoolManager.Instance.GetLastAnimator();
            mEnemyNode[enemyobject].SetNode(anim, enemy, enemyStats[enemyobject].NodeCharacterStats, 1);
        }
    }

    /// <summary>
    /// 讀取角色
    /// </summary>
    void LoadAlly(BattleNode[] monsterStats)
    {
        for (int enemyobject = 0; enemyobject < monsterStats.Length; enemyobject++)
        {
            if (mAllyNode[enemyobject].Character != null)
            {
                Debug.Log("已有角色");
                continue;
            }
            GameObject enemy = Instantiate(ObjectPoolManager.Instance.GetCharacter(mAllyNode[enemyobject].NodeCharacterStats.CharacterNumber), mAllyNode[enemyobject].transform.position, mAllyNode[enemyobject].transform.rotation);
            Animator anim = ObjectPoolManager.Instance.GetLastAnimator();
            enemy.name = mAllyNode[enemyobject].NodeCharacterStats.Name;
            mAllyNode[enemyobject].SetAllyNode(anim, enemy, 0);
        }
    }

    /// <summary>
    /// 讀取UI，並在ObjectPool中搜尋有幾個可以用的技能(目前只有攻擊)
    /// </summary>
    void LoadBattleUI()
    {
        ObjectPoolManager.Instance.GetObjectInGame("BattleUICanvas").SetActive(true);
    }
}
