﻿using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;
using UnityEngine;
using System.Collections;

// 1.將data改成由網路讀取
// 2.將Data改成Binary(編輯器)
// 3.將Data改成一次讀n個byte
public class BattleStatsSave : ISaveLoadInterface
{
    public void Save(string path, string name)
    {
        var binarayformatter = new BinaryFormatter();
        using (var stream = new FileStream(path + name + SaveFileFormat.SaveFromat(eSaveFileFormat.json), FileMode.Create))
        {
            var data = new MobStats();
            binarayformatter.Serialize(stream, data);
        }
    }

    public void Load(string path)
    {
        // 隱藏由HTTP的讀取模式
        //BattleManager.Instance.StartCoroutine(DownloadMonsterdbJsonData(path));
        using (var stream = new StreamReader(path))
        {
            FileLoadMonsterdbJsonData(stream);
        }
    }

    void FileLoadMonsterdbJsonData(StreamReader stream)
    {
        MobStats jsondata = JsonUtility.FromJson<MobStats>(stream.ReadToEnd());
        for (int charactercount = 0; charactercount < jsondata.MonsterStats.Length; charactercount++)
        {
            MonsterdbDic.Instance.SetMonsterIntDic(charactercount, jsondata.MonsterStats[charactercount]);
        }
    }

    IEnumerator DownloadMonsterdbJsonData(string path)
    {
        using (var request = UnityEngine.Networking.UnityWebRequest.Get(path))
        {
            yield return request.SendWebRequest();
            MobStats jsondata = JsonUtility.FromJson<MobStats>(request.downloadHandler.text);
            for (int charactercount = 0; charactercount < jsondata.MonsterStats.Length; charactercount++)
            {
                MonsterdbDic.Instance.SetMonsterIntDic(charactercount, jsondata.MonsterStats[charactercount]);
            }
        }
    }

    public void LoadPlayerStats(string path)
    {
        // 隱藏由HTTP的讀取模式
        //BattleManager.Instance.StartCoroutine(DownloadMonsterdbJsonData(path));
        using (var stream = new StreamReader(path))
        {
            FileloadPlayerdbJsonData(stream);
        }
        //BattleManager.Instance.StartCoroutine(DownloadPlayerdbJsonData(path));
    }

    public void FileloadPlayerdbJsonData(StreamReader stream)
    {
        MobStats jsondata = JsonUtility.FromJson<MobStats>(stream.ReadToEnd());
        if (jsondata.MonsterStats.Length != BattleManager.MAX_CHARACTER)
        {
            Debug.LogError("錯誤的玩家資料，長度 : " + jsondata.MonsterStats.Length);
        }
        BattleManager.Instance.SetPlayerStats(jsondata.MonsterStats);
    }

    IEnumerator DownloadPlayerdbJsonData(string path)
    {
        using (var request = UnityEngine.Networking.UnityWebRequest.Get(path))
        {
            yield return request.SendWebRequest();
            MobStats jsondata = JsonUtility.FromJson<MobStats>(request.downloadHandler.text);
            if (jsondata.MonsterStats.Length != BattleManager.MAX_CHARACTER)
            {
                Debug.LogError("錯誤的玩家資料，長度 : " + jsondata.MonsterStats.Length);
                yield return null;
            }
            BattleManager.Instance.SetPlayerStats(jsondata.MonsterStats);
        }
    }
}

[Serializable]
public class MobStats
{
    [Serializable]
    public struct sMonsterStats
    {
        public float Attack
        {
            get
            {
                return mAttack;
            }
        }

        public float Defend
        {
            get
            {
                return mDefend;
            }
        }

        public float MaxHp
        {
            get
            {
                return mMaxHp;
            }
        }

        public string MonsterName
        {
            get
            {
                return mMonsterName;
            }

            set
            {
                mMonsterName = value;
            }
        }

        public int MonsterNumber
        {
            get
            {
                return mMonsterNumber;
            }

            set
            {
                mMonsterNumber = value;
            }
        }

        [SerializeField] float mMaxHp;
        [SerializeField] float mDefend;
        [SerializeField] float mAttack;
        [SerializeField] string mMonsterName;
        [SerializeField] int mMonsterNumber;
    }

    public sMonsterStats[] MonsterStats
    {
        get
        {
            return mCharacterStats;
        }
    }

    [SerializeField]
    sMonsterStats[] mCharacterStats;
}
