﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 儲存怪物資料，並再戰鬥開始時從Dictionaty中找是幾號怪物的Data，並回傳屬性，目前屬性跟怪物模組分開，可以同一個屬性資料，但是模組不同
/// 屬性 : 攻擊、血量 等資訊
/// </summary>
public class MonsterdbDic : MonoBehaviour
{
    public static MonsterdbDic Instance
    {
        get
        {
            if (mInstance == null)
            {
                var gamemanager = new GameObject("MonsterdbDic");
                gamemanager.AddComponent<MonsterdbDic>();
            }
            return mInstance;
        }
    }

    Dictionary<int, MobStats.sMonsterStats> mMonsterIntDic = new Dictionary<int, MobStats.sMonsterStats>();

    static MonsterdbDic mInstance;

    public MobStats.sMonsterStats GetMonsterIntDic(int monsterNumber)
    {
        if (!mMonsterIntDic.ContainsKey(monsterNumber))
        {
            Debug.LogError("這個代碼沒有怪物");
            Debug.Log(monsterNumber);
            return mMonsterIntDic[1];
        }
        else
        {
            return mMonsterIntDic[monsterNumber];
        }
    }

    public void SetMonsterIntDic(int monsterInt, MobStats.sMonsterStats monsterStats)
    {
        if (mMonsterIntDic.ContainsKey(monsterInt))
        {
            Debug.LogError("已有此Key");
            return;
        }
        else
        {
            mMonsterIntDic.Add(monsterInt, monsterStats);
        }
    }

    void Awake()
    {
        mInstance = this;
    }
}
