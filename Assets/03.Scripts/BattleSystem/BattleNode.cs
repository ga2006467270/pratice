﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleNode : MonoBehaviour
{
    const float DESTROY_TIME = 0.5f;

    public GameObject Character
    {
        get
        {
            return mCharacter;
        }

        set
        {
            mCharacter = value;
        }
    }

    public CharacterStats NodeCharacterStats
    {
        get
        {
            return mNodeCharacterStats;
        }

        set
        {
            mNodeCharacterStats = value;
        }
    }

    public Animator Anim
    {
        get
        {
            return mAnim;
        }

        set
        {
            mAnim = value;
        }
    }

    public bool IsTurn
    {
        get
        {
            return mIsTurn;
        }

        set
        {
            mIsTurn = value;
        }
    }

    public bool WasTurnPrev
    {
        get
        {
            return mWasTurnPrev;
        }
    }

    public int Group
    {
        get
        {
            return mGroup;
        }

        set
        {
            mGroup = value;
        }
    }

    public bool Down
    {
        get
        {
            return mDown;
        }
    }

    CharacterStats mNodeCharacterStats;
    GameObject mCharacter;
    Animator mAnim;
    bool mIsTurn;
    bool mWasTurnPrev = false;
    bool mDown;
    int mGroup;

    public void SetNode(Animator anim, GameObject character, CharacterStats characterStats, int group)
    {
        mAnim = anim;
        mCharacter = character;
        mNodeCharacterStats = characterStats;
        mGroup = group;
    }

    public void SetAllyNode(Animator anim, GameObject character, int group)
    {
        mAnim = anim;
        mCharacter = character;
        mGroup = group;
        NodeCharacterStats.CurrentHp = NodeCharacterStats.MaxHp;
    }

    public void ResetTurn()
    {
        mWasTurnPrev = false;
    }

    public void ResetNode()
    {
        mCharacter = null;
        mDown = false;
    }

    public void CharacterDown()
    {
        Anim.SetTrigger("Dead");
        Anim.SetBool("Down", true);
        mDown = true;
    }

    /// <summary>
    /// 移動至敵人前方，進行攻擊，並回到原位
    /// </summary>
    /// <returns></returns>
    public IEnumerator DoTurn()
    {
        StartCoroutine(Attack());
        while (mIsTurn == true)
        {
            yield return new WaitForSeconds(0.25f);
        }
        Debug.Log("戰鬥動作完畢");
        mIsTurn = false;
        mWasTurnPrev = true;
        BattleManager.Instance.NextCharacterTurn();
    }

    void Awake()
    {
        BattleManager.Instance.AddToBattleNodeToArray(this, this.name);
    }

    IEnumerator Attack()
    {
        Vector3 origianlpos = mCharacter.transform.position;
        BattleNode target = BattleManager.Instance.GetTarget(mGroup);
        if (target == null)
        {
            StartCoroutine(BattleManager.Instance.LeaveTheBattle());
            yield return null;
        }
        else
        {
            mCharacter.transform.DOMove(target.Character.transform.position + target.Character.transform.forward, 0.5f).SetEase(Ease.InOutQuad);
            yield return new WaitForSeconds(0.5f);
            mAnim.SetTrigger("Hit");
            BattleManager.Instance.IsInAnimation = true;
            while (BattleManager.Instance.IsInAnimation == true)
            {
                yield return new WaitForFixedUpdate();
            }
            float totalDamage = Mathf.Clamp(mNodeCharacterStats.AttackValue - target.mNodeCharacterStats.DefendValue, 1, Mathf.Infinity);
            if (totalDamage == 0)
            {
                Debug.Log("防禦高於攻擊");
            }
            target.NodeCharacterStats.CurrentHp -= totalDamage;
            Debug.Log("剩餘血量 : " + target.NodeCharacterStats.CurrentHp);
            yield return new WaitForSeconds(0.1f);
            mCharacter.transform.DOMove(origianlpos, 0.5f).SetEase(Ease.InOutQuad);
            yield return new WaitForSeconds(0.3f);
            mIsTurn = false;
        }
    }
}
