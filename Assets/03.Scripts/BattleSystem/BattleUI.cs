﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class BattleUI : MonoBehaviour
{
    const float SMOOTH_SEC = 0.5f;

    public static BattleUI Instance
    {
        get
        {
            if (mInstance == null)
            {
                var gamemanager = new GameObject("BattleManager");
                gamemanager.AddComponent<BattleManager>();
            }
            return mInstance;
        }
    }

    static BattleUI mInstance;
    [SerializeField] Image mBlackScreenImage;
    [SerializeField] GameObject BattlePanel;
    float mBlackImangeCounter;
    public void Attack()
    {
        BattleManager.Instance.Attack();
    }

    /// <summary>
    /// 切換場景時黑掉螢幕再亮起
    /// </summary>
    public void ChangeBattleSmooth()
    {
        StartCoroutine(BattleSmoothCoroutine());
    }

    void Awake()
    {
        mInstance = this;
        CloseBattleUI();
    }

    public void ActiveBattleUI()
    {
        BattlePanel.SetActive(true);
    }

    public void CloseBattleUI()
    {
        BattlePanel.SetActive(false);
    }

    /// <summary>
    /// 轉換場景變黑
    /// </summary>
    /// <returns></returns>
    IEnumerator BattleSmoothCoroutine()
    {
        ActiveBattleUI();
        if (mBlackImangeCounter < SMOOTH_SEC)
        {
            mBlackImangeCounter += Time.deltaTime;
            float count = 1 - (mBlackImangeCounter / SMOOTH_SEC);
            mBlackScreenImage.color = new Color(mBlackScreenImage.color.r, mBlackScreenImage.color.g, mBlackScreenImage.color.b, count);
            yield return new WaitForFixedUpdate();
            StartCoroutine(BattleSmoothCoroutine());
        }
        else
        {
            mBlackImangeCounter = 0;
            yield return null;
        }
    }
}
