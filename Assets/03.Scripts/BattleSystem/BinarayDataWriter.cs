﻿using System.Collections;
using System.IO;

public class BinaryDataWriter
{
    public static void WriteBinary(BinaryWriter writer, int content)
    {
        int value = 1;
        writer.Write(value);
        writer.Write(content);
    }

    public static void WriteBinary(BinaryWriter writer, float content)
    {
        int value = 2;
        writer.Write(value);
        writer.Write(content);
    }

    public static void WriteBinary(BinaryWriter writer, bool[] content)
    {
        int value = 3;

        BitArray bit = new BitArray(content);
        byte[] bytes = new byte[1];
        bit.CopyTo(bytes, 0);

        writer.Write(value);
        writer.Write(bytes);
    }

    public static void WriteBinary(BinaryWriter writer, string content)
    {
        int value = 4;
        writer.Write(value);
        writer.Write(content);
    }
}