﻿using UnityEngine;

public interface IObjectLoadType
{
    void LoadObject();
    void ChangeCameraType();
}

public class InstanciatateObject
{
    public static void InstantiateObject(string objectName)
    {
        /// <summary>
        /// 若場景上無此名子的物件 則創造一個物件
        /// </summary>
        if (ObjectPoolManager.Instance.GetObjectInGame(objectName) == null)
        {
            GameObject poolObject = ObjectPoolManager.Instance.GetObjectFromPool(objectName);
            ObjectPoolManager.Instance.NewInstantiateObject(poolObject);
        }
        else
        {
            Debug.Log("場景中已有 : " + objectName);
            return;
        }
    }
}

public class ObjectLoadMove : IObjectLoadType
{
    public void LoadObject()
    {
        InstanciatateObject.InstantiateObject("MainCamera");
        InstanciatateObject.InstantiateObject("DirectionalLight");
        InstanciatateObject.InstantiateObject("EventSystem");
        InstanciatateObject.InstantiateObject("Ground");
        InstanciatateObject.InstantiateObject("Hero");
        InstanciatateObject.InstantiateObject("JoyStickCanvas");
        InstanciatateObject.InstantiateObject("DectectGrid");
        InstanciatateObject.InstantiateObject("Enviroment");
        ChangeCameraType();
    }

    public void ChangeCameraType()
    {
        CameraController.Instance.ChangeCameraType(CameraController.eSystemType.KeyBoard);
    }
}

public class ObjectLoadPlantNPCSave : IObjectLoadType
{
    public void LoadObject()
    {
        InstanciatateObject.InstantiateObject("MainCamera");
        InstanciatateObject.InstantiateObject("DirectionalLight");
        InstanciatateObject.InstantiateObject("EventSystem");
        InstanciatateObject.InstantiateObject("Ground");
        InstanciatateObject.InstantiateObject("PlantNPCSaveCanvas");
        InstanciatateObject.InstantiateObject("DectectGrid");
        InstanciatateObject.InstantiateObject("Enviroment");
        ChangeCameraType();
    }

    public void ChangeCameraType()
    {
        CameraController.Instance.ChangeCameraType(CameraController.eSystemType.KeyBoard);
    }
}

public class ObjectLoadNPCInteract : IObjectLoadType
{
    public void LoadObject()
    {
        InstanciatateObject.InstantiateObject("Ground");
        InstanciatateObject.InstantiateObject("MainCamera");
        InstanciatateObject.InstantiateObject("DirectionalLight");
        InstanciatateObject.InstantiateObject("EventSystem");
        InstanciatateObject.InstantiateObject("Hero");
        InstanciatateObject.InstantiateObject("JoyStickCanvas");
        InstanciatateObject.InstantiateObject("NPCInteractCanvas");
        InstanciatateObject.InstantiateObject("DectectGrid");
        InstanciatateObject.InstantiateObject("Enviroment");
        ChangeCameraType();
    }

    public void ChangeCameraType()
    {
        CameraController.Instance.ChangeCameraType(CameraController.eSystemType.KeyBoard);
    }
}

public class ObjectLoadNPCInteractPlant : IObjectLoadType
{
    public void LoadObject()
    {
        InstanciatateObject.InstantiateObject("MainCamera");
        InstanciatateObject.InstantiateObject("DirectionalLight");
        InstanciatateObject.InstantiateObject("EventSystem");
        InstanciatateObject.InstantiateObject("Ground");
        InstanciatateObject.InstantiateObject("DectectGrid");
        InstanciatateObject.InstantiateObject("NPCInteractPlantCanvas");
        InstanciatateObject.InstantiateObject("Enviroment");
        ChangeCameraType();
    }

    public void ChangeCameraType()
    {
        CameraController.Instance.ChangeCameraType(CameraController.eSystemType.KeyBoard);
    }
}

public class ObjectLoadAssetBundle : IObjectLoadType
{
    public void LoadObject()
    {
        InstanciatateObject.InstantiateObject("MainCamera");
        InstanciatateObject.InstantiateObject("DirectionalLight");
        InstanciatateObject.InstantiateObject("EventSystem");
#if UNITY_EDITOR
        InstanciatateObject.InstantiateObject("AssetBundleLoadCanvas");
#endif
#if !UNITY_EDITOR
        InstanciatateObject.InstantiateObject("AssetBundleLoadAndriodCanvas");
#endif
        ChangeCameraType();
    }

    public void ChangeCameraType()
    {
        CameraController.Instance.ChangeCameraType(CameraController.eSystemType.None);
    }
}

public class ObjectLoadBattleSystem : IObjectLoadType
{
    public void LoadObject()
    {
        InstanciatateObject.InstantiateObject("MainCamera");
        InstanciatateObject.InstantiateObject("DirectionalLight");
        InstanciatateObject.InstantiateObject("EventSystem");
        InstanciatateObject.InstantiateObject("Ground");
        InstanciatateObject.InstantiateObject("Hero");
        InstanciatateObject.InstantiateObject("JoyStickCanvas");
        InstanciatateObject.InstantiateObject("NPCInteractCanvas");
        InstanciatateObject.InstantiateObject("DectectGrid");
        InstanciatateObject.InstantiateObject("BattleUICanvas");
        InstanciatateObject.InstantiateObject("BattleField");
        InstanciatateObject.InstantiateObject("EnemyField");
        InstanciatateObject.InstantiateObject("AllyField");
        InstanciatateObject.InstantiateObject("Enviroment");
        BattleManager.Instance.ChangeBattleObject();
        ChangeCameraType();
    }

    public void ChangeCameraType()
    {
        CameraController.Instance.ChangeCameraType(CameraController.eSystemType.Follow);
    }
}