﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 物件池
/// </summary>
public class ObjectPoolManager : MonoBehaviour
{
    public enum eSystemType
    {
        PlantNPCSave,
        Move,
        NPCInteract,
        NPCExcelPlant,
        LoadAssetBundle,
        BattleSystem,
    }

    public enum eObjectPoolType
    {
        ChracterPool,
        ObjectPool,
        TexturePool,
        MaterialPool,
    }

    public static ObjectPoolManager Instance
    {
        get
        {
            if (mInstance != null)
            {
                return mInstance;
            }
            mInstance = FindObjectOfType<ObjectPoolManager>();
            return mInstance;
        }
    }

    public List<GameObject> CharacterObjectPool
    {
        get
        {
            return mCharacterObjectPool;
        }
        set
        {
            mCharacterObjectPool = value;
        }
    }

    public eSystemType SystemType
    {
        get
        {
            return mSystemType;
        }

        set
        {
            mSystemType = value;
        }
    }

    [SerializeField] eSystemType mSystemType;
    Dictionary<string, GameObject> mObjectInGame = new Dictionary<string, GameObject>();
    Dictionary<string, GameObject> mObjectPoolDic = new Dictionary<string, GameObject>();
    Dictionary<string, Texture2D> mObjectStringTextDic = new Dictionary<string, Texture2D>();
    Dictionary<string, Material> mObjectStringMaterialDic = new Dictionary<string, Material>();
    Dictionary<string, Animator> mObjectStringAnimatorDic = new Dictionary<string, Animator>();
    Dictionary<GameObject, int> mCharacterObjectToInt = new Dictionary<GameObject, int>();
    Dictionary<int, GameObject> mCharacterIntToObject = new Dictionary<int, GameObject>();
    [SerializeField] List<Texture2D> mTextPool = new List<Texture2D>();
    [SerializeField] List<GameObject> mObjectPool = new List<GameObject>();
    [SerializeField] List<GameObject> mCharacterObjectPool = new List<GameObject>();
    [SerializeField] List<Material> mMaterialPool = new List<Material>();
    [SerializeField] List<Animator> mAnimatorPool = new List<Animator>();
    static ObjectPoolManager mInstance;
    ObjectStore mFactoryStore;

    public void LoadObjectPool()
    {
        mFactoryStore = new ObjectStore(new SimpleObjectFactory());
        mFactoryStore.ObjectOrder(mSystemType);
    }

    public void LoadObjectPoolToGame(eSystemType systemType)
    {
        mFactoryStore = new ObjectStore(new SimpleObjectFactory());
        mFactoryStore.ObjectOrder(systemType);
    }

    public void AddAnimator(Animator anim)
    {
        if (mAnimatorPool.Contains(anim))
        {
            Debug.Log("已有此物件 : " + anim);
            return;
        }
        mAnimatorPool.Add(anim);
        if (mObjectStringAnimatorDic.ContainsKey(name))
        {
            mObjectStringAnimatorDic[name] = anim;
        }
        else
        {
            mObjectStringAnimatorDic.Add(name, anim);
        }
    }

    public Animator GetLastAnimator()
    {
        return mAnimatorPool[mAnimatorPool.Count - 1];
    }

    /// <summary>
    /// 將物件加入Dictionary
    /// </summary>
    /// <param name="name">物件名稱</param>
    public void AddObject(GameObject loadObject, string name)
    {
        if (mObjectPool.Contains(loadObject))
        {
            Debug.Log("已有此物件 : " + loadObject);
            return;
        }
        mObjectPool.Add(loadObject);
        if (mObjectPoolDic.ContainsKey(name))
        {
            mObjectPoolDic[name] = loadObject;
        }
        else
        {
            mObjectPoolDic.Add(name, loadObject);
        }
    }

    /// <summary>
    /// 將Texture加入Dictionary
    /// </summary>
    /// <param name="name">物件名稱</param>
    public void AddText(Texture2D loadText, string name)
    {
        if (mTextPool.Contains(loadText))
        {
            Debug.Log("已有此物件 : " + loadText);
            return;
        }
        mTextPool.Add(loadText);
        mObjectStringTextDic.Add(name, loadText);
    }

    /// <summary>
    /// 設定腳色Dictionary
    /// </summary>
    public void ReSetCharacter()
    {
        mCharacterObjectToInt.Clear();
        mCharacterIntToObject.Clear();
        for (int character = 0; character < CharacterObjectPool.Count; character++)
        {
            mCharacterObjectToInt.Add(CharacterObjectPool[character], character + 1);
            mCharacterIntToObject.Add(character + 1, CharacterObjectPool[character]);
        }
        Debug.Log("重整");
    }

    public void AddCharacter(GameObject character)
    {
        if (!CharacterObjectPool.Contains(character))
        {
            CharacterObjectPool.Add(character);
            Debug.Log("加入腳色");
        }
    }

    public void AddMaterial(Material material, string materialname)
    {
        if (!mMaterialPool.Contains(material))
        {
            mMaterialPool.Add(material);
            mObjectStringMaterialDic.Add(materialname, material);
        }
    }

    /// <summary>
    /// 在物件池中尋找物品
    /// </summary>
    /// <param name="objectName"></param>
    /// <returns></returns>
    public GameObject GetObjectFromPool(string objectName)
    {
        if (mObjectPoolDic.ContainsKey(objectName))
        {
            return mObjectPoolDic[objectName];
        }
        else
        {
            Debug.LogError("mObjectPoolDic中沒有 : " + objectName);
            return new GameObject();
        }
    }

    /// <summary>
    /// Text物件池中尋找物品
    /// </summary>
    /// <param name="objectName"></param>
    /// <returns></returns>
    public Texture2D GetObjectFromTextPool(string objectName)
    {
        if (mObjectStringTextDic.ContainsKey(objectName))
        {
            return mObjectStringTextDic[objectName];
        }
        else
        {
            Debug.LogError("沒有此Texture : " + objectName);
            return new Texture2D(1, 1);
        }
    }

    public Material GetMaterialFromMaterialPool(string materialName)
    {
        if (mObjectStringMaterialDic.ContainsKey(materialName))
        {
            return mObjectStringMaterialDic[materialName];
        }
        else
        {
            Debug.LogError("沒有此materialName : " + materialName);
            return null;
        }
    }

    /// <summary>
    /// 尋找在場景中的物件
    /// </summary>
    /// <param name="stringName"></param>
    /// <returns></returns>
    public GameObject GetObjectInGame(string stringName)
    {
        if (mObjectInGame.ContainsKey(stringName))
        {
            return mObjectInGame[stringName];
        }
        else
        {
            Debug.Log("場景中沒有 : " + stringName + " 物件");
            return null;
        }
    }

    public void ClearObjectInGame()
    {
        foreach (KeyValuePair<string, GameObject> objectingame in mObjectInGame)
        {
            Destroy(objectingame.Value);
        }
        mObjectInGame.Clear();
    }

    /// <summary>
    /// 將場景中物件加入Dictionary
    /// </summary>
    /// <param name="objectName"></param>
    /// <param name="gameObject"></param>
    public void NewInstantiateObject(GameObject poolObject)
    {
        GameObject theobject = Object.Instantiate(poolObject, poolObject.transform.position, poolObject.transform.rotation);
        theobject.name = poolObject.name;
        mObjectInGame.Add(poolObject.name, theobject);
    }

    public int GetCharacter(GameObject character)
    {
        if (mCharacterObjectToInt.ContainsKey(character))
        {
            return mCharacterObjectToInt[character];
        }
        else
        {
            Debug.LogError("沒有 : " + character + "對應的號碼");
            return 0;
        }
    }

    public GameObject GetCharacter(int characterIndex)
    {
        if (mCharacterIntToObject.ContainsKey(characterIndex))
        {
            return mCharacterIntToObject[characterIndex];
        }
        else
        {
            Debug.LogError("沒有 : " + characterIndex + "號的角色");
            return new GameObject();
        }
    }

    public void SetSerializeObjectPool()
    {
        foreach (GameObject objectinpool in mObjectPool)
        {
            if (mObjectPoolDic.ContainsKey(objectinpool.name))
            {
                mObjectPoolDic[objectinpool.name] = objectinpool;
            }
            else
            {
                mObjectPoolDic.Add(objectinpool.name, objectinpool);
                Debug.Log("加入" + objectinpool.name);
            }
        }

        foreach (Texture2D textinpool in mTextPool)
        {
            if (mObjectStringTextDic.ContainsKey(textinpool.name))
            {
                mObjectStringTextDic[textinpool.name] = textinpool;
            }
            else
            {
                mObjectStringTextDic.Add(textinpool.name, textinpool);
                Debug.Log("加入" + textinpool.name);
            }
        }

        foreach (Material materialinpool in mMaterialPool)
        {
            if (mObjectStringMaterialDic.ContainsKey(materialinpool.name))
            {
                mObjectStringMaterialDic[materialinpool.name] = materialinpool;
            }
            else
            {
                mObjectStringMaterialDic.Add(materialinpool.name, materialinpool);
                Debug.Log("加入" + materialinpool.name);
            }
        }
    }

    void Awake()
    {
        mInstance = this;
    }

}

public class ObjectStore
{
    SimpleObjectFactory mFactory;

    public ObjectStore(SimpleObjectFactory factory)
    {
        mFactory = factory;
    }

    public void ObjectOrder(ObjectPoolManager.eSystemType objectEnum)
    {
        IObjectLoadType objectloadtype;
        objectloadtype = mFactory.CreateObject(objectEnum);
        objectloadtype.LoadObject();
    }
}

public class SimpleObjectFactory
{
    public IObjectLoadType CreateObject(ObjectPoolManager.eSystemType objectType)
    {
        switch (objectType)
        {
            case ObjectPoolManager.eSystemType.Move:
                return new ObjectLoadMove();
            case ObjectPoolManager.eSystemType.PlantNPCSave:
                return new ObjectLoadPlantNPCSave();
            case ObjectPoolManager.eSystemType.NPCInteract:
                return new ObjectLoadNPCInteract();
            case ObjectPoolManager.eSystemType.NPCExcelPlant:
                return new ObjectLoadNPCInteractPlant();
            case ObjectPoolManager.eSystemType.LoadAssetBundle:
                return new ObjectLoadAssetBundle();
            case ObjectPoolManager.eSystemType.BattleSystem:
                return new ObjectLoadBattleSystem();
        }
        return null;
    }
}
