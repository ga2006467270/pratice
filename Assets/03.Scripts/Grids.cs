﻿using System.Collections.Generic;
using UnityEngine;

public class Grids : MonoBehaviour
{
    public int MaxSize
    {
        get
        {
            return mGridSizeX * mGridSizeY;
        }
    }

    public static Grids Instance
    {
        get
        {
            if (_instance == null)
            {
                GameObject gamemanager = new GameObject("Grid");
                gamemanager.AddComponent<Grids>();
            }
            return _instance;
        }
    }

    public int Bound
    {
        get
        {
            return mBound;
        }
        set
        {
            mBound = value;
        }
    }

    public float NodeRaius
    {
        get
        {
            return mNodeRadius;
        }
        set
        {
            mNodeRadius = value;
        }
    }

    public float DetectOffset
    {
        get
        {
            return mDetectOffset;
        }
        set
        {
            mDetectOffset = value;
        }
    }

    [SerializeField] LayerMask UnWakableMask = 1 << 9;
    [SerializeField] LayerMask GroundMask = 1 << 8;

    protected int mGridSizeX, mGridSizeY;
    protected float mNodeDiameter;

    int mBound = 150;
    float mNodeRadius = 1;
    float mDetectOffset = 0.5f;
    static Grids _instance;

    public Node[,] NodeArray
    {
        get
        {
            return mNodeArray;
        }
        set
        {
            mNodeArray = value;
        }
    }

    Node[,] mNodeArray;

    public virtual void Awake()
    {
        _instance = this;
    }

    public void Start()
    {
        SetNode();
    }

    public void SetNode()
    {
        mGridSizeX = Mathf.RoundToInt(Bound / NodeRaius);
        mGridSizeY = Mathf.RoundToInt(Bound / NodeRaius);
        mNodeDiameter = NodeRaius / 2;
        CreateTheNode();
    }

    public void ResetNode()
    {
        Vector3 worldbottomleft = transform.position - Vector3.right * Bound / 2 - Vector3.forward * Bound / 2;
        for (int gridsizecountx = 0; gridsizecountx < Bound; gridsizecountx++)
        {
            for (int gridsizecounty = 0; gridsizecounty < Bound; gridsizecounty++)
            {
                Vector3 worldpoint = worldbottomleft + Vector3.right * (gridsizecountx * NodeRaius + mNodeDiameter) + Vector3.forward * (gridsizecounty * NodeRaius + mNodeDiameter) + Vector3.up * DetectOffset;
                bool walkable = !Physics.CheckSphere(worldpoint, NodeRaius, UnWakableMask);
                NodeArray[gridsizecountx, gridsizecounty].Walkable = walkable;
            }
        }
    }

    public virtual void CreateTheNode()
    {
        NodeArray = new Node[mGridSizeX, mGridSizeY];
        Vector3 worldbottomleft = transform.position - Vector3.right * Bound / 2 - Vector3.forward * Bound / 2;
        for (int gridsizecountx = 0; gridsizecountx < Bound; gridsizecountx++)
        {
            for (int gridsizecounty = 0; gridsizecounty < Bound; gridsizecounty++)
            {
                Vector3 worldpoint = worldbottomleft + Vector3.right * (gridsizecountx * NodeRaius + mNodeDiameter) + Vector3.forward * (gridsizecounty * NodeRaius + mNodeDiameter) + Vector3.up * DetectOffset;
                bool walkable = !Physics.CheckSphere(worldpoint, NodeRaius, UnWakableMask);
                Node plantnode = new Node();
                if (walkable)
                {
                    RaycastHit hit;
                    Ray ray = new Ray(worldpoint, Vector3.down * DetectOffset);
                    Physics.Raycast(ray, out hit, GroundMask);
                    plantnode.InitializeNode(worldpoint, walkable, gridsizecountx, gridsizecounty, hit.point.y);
                }
                else
                {
                    plantnode.InitializeNode(worldpoint, walkable, gridsizecountx, gridsizecounty, 0);
                }
                NodeArray[gridsizecountx, gridsizecounty] = plantnode;
            }
        }
    }

    public Node NodeFromWorldPoint(Vector3 worldPosition)
    {
        float percentx = (worldPosition.x - transform.position.x) / Bound + 0.5f - (mNodeDiameter / Bound);
        float percenty = (worldPosition.z - transform.position.z) / Bound + 0.5f - (mNodeDiameter / Bound);

        percentx = Mathf.Clamp01(percentx);
        percenty = Mathf.Clamp01(percenty);

        int numberx = Mathf.RoundToInt((mGridSizeX - 1) * percentx);
        int numbery = Mathf.RoundToInt((mGridSizeY - 1) * percenty);

        return NodeArray[numberx, numbery];
    }

    public List<INode> GetNeighbor(INode node)
    {
        List<INode> neighbornode = new List<INode>();
        for (int neighborxcount = -1; neighborxcount <= 1; neighborxcount++)
        {
            for (int neighborycount = -1; neighborycount <= 1; neighborycount++)
            {
                if (neighborxcount == 0 && neighborycount == 0)
                {
                    continue;
                }
                int neighborx = node.Numberx + neighborxcount;
                int neighbory = node.Numbery + neighborycount;
                if (neighborx >= 0 && neighborx < mGridSizeX && neighbory >= 0 && neighbory < mGridSizeY)
                    neighbornode.Add(NodeArray[neighborx, neighbory]);
            }
        }
        return neighbornode;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.gray;
        if (mNodeArray != null)
        {
            foreach (Node node in mNodeArray)
            {
                if (!node.Walkable)
                {
                    Gizmos.color = Color.red;
                    Gizmos.DrawWireCube(node.Pos, Vector3.one * 0.9f);
                }
            }
        }
    }

}
