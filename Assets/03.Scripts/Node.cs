﻿using UnityEngine;

public interface INode
{
    Vector3 Pos { get; set; }
    bool Walkable { get; set; }
    int Numberx { get; set; }
    int Numbery { get; set; }
    float NumberHight { get; set; }

    void InitializeNode(Vector3 pos, bool walkable, int numberx, int numbery, float numberz);
}
