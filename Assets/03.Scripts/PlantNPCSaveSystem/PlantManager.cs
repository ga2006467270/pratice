﻿using UnityEngine;

public class PlantManager : MonoBehaviour
{
    public enum eEditorMode
    {
        Add,
        Delete,
        Move,
        Edit,
    }

    public eEditorMode EditorMode
    {
        get
        {
            return mEditorMode;
        }
        set
        {
            mEditorMode = value;
            CurrentNode = new Node();
        }
    }

    public Node CurrentNode
    {
        get
        {
            return mCurrentNode;
        }
        set
        {
            mCurrentNode = value;
            if (CurrentNode.HasGameObject != null)
            {
                NPCSaveUIManager.Instance.SetEditorValue();
                GridDetectHelper.Instance.DetectGridMat.color = Color.red;
            }
            else
            {
                GridDetectHelper.Instance.DetectGridMat.color = Color.white;
            }
        }
    }

    public static PlantManager Instance
    {
        get
        {
            if (mInstance == null)
            {
                var gamemanager = new GameObject("PlantManager");
                gamemanager.AddComponent<PlantManager>();
            }
            return mInstance;
        }
    }

    LayerMask GroundLayer = 1 << 8;
    [HideInInspector] public int CurrentAngle = 0;
    [HideInInspector] public GameObject CurrentCharacter;

    [SerializeField] eEditorMode mEditorMode;
    static PlantManager mInstance;
    IPlantType mPCPlant;
    Node mCurrentNode;

    /// <summary>
    /// 目前模式
    /// </summary>
    /// <param name="node">互動node</param>
    /// <param name="interactPosition">目標點</param>
    public void DoWithNode(Node node, Vector3 interactPosition)
    {
        switch (EditorMode)
        {
            case eEditorMode.Add:
                AddCharacter(node, interactPosition);
                break;
            case eEditorMode.Delete:
                DeleteCharacter(node);
                break;
            case eEditorMode.Move:
                MoveCharacter(node, interactPosition);
                break;
            case eEditorMode.Edit:
                EditorCharacter(node);
                break;
        }
    }

    /// <summary>
    /// Excel檔案轉換為Node資料
    /// </summary>
    /// <param name="npcData">npc資料</param>
    public void ExcelTurnToPlantNode(SaveLoadStructure.sNPC[] npcData)
    {

        for (int datalength = 0; datalength < npcData.Length; datalength++)
        {
            if (npcData[datalength].Positionx > Grids.Instance.Bound || npcData[datalength].Positiony > Grids.Instance.Bound)
            {
                Debug.LogError("這個Postion超出範圍");
                return;
            }
            if (Grids.Instance.NodeArray[npcData[datalength].Positionx, npcData[datalength].Positiony].HasGameObject == true)
            {
                Debug.LogError("請問填寫重複的座標");
                return;
            }
            Node node = Grids.Instance.NodeArray[npcData[datalength].Positionx, npcData[datalength].Positiony];
            node.SerialNumber = npcData[datalength].SerialNumber;
            node.CharacterNumber = npcData[datalength].CharacterNumber;
            node.Sex = npcData[datalength].Sex;
            node.Content = npcData[datalength].Content;
            node.HasGameObject = Instantiate(ObjectPoolManager.Instance.GetCharacter(node.CharacterNumber), new Vector3(node.Pos.x, node.NumberHight, node.Pos.z), Quaternion.Euler(0, CurrentAngle, 0));
            node.Rotation = CurrentAngle;
            node.HasGameObject.name = npcData[datalength].Name;
        }
    }

    /// <summary>
    /// 編輯模式
    /// </summary>
    /// <param name="node">當前node</param>
    public void EditorCharacter(Node node)
    {
        if (node.HasGameObject)
        {
            Add(node);
        }
    }

    /// <summary>
    /// 目標Node加入GameObject
    /// </summary>
    /// <param name="node">目標node</param>
    public void Add(Node node)
    {
        CurrentNode = node;
    }

    void Awake()
    {
        mInstance = this;
    }

    void Start()
    {
        CurrentNode = new Node();
        mPCPlant = new PCPlant(GroundLayer);

        if (ObjectPoolManager.Instance.CharacterObjectPool.Count > 0)
        {
            CurrentCharacter = ObjectPoolManager.Instance.CharacterObjectPool[0];
        }
        else
        {
            Debug.Log("沒有");
        }

        ObjectPoolManager.Instance.ReSetCharacter();
    }

    void Update()
    {
        mPCPlant.Plant();
    }

    /// <summary>
    /// 新增NPC
    /// </summary>
    /// <param name="node">新增node</param>
    /// <param name="interactPosition">目標點</param>
    void AddCharacter(Node node, Vector3 interactPosition)
    {
        if (node.HasGameObject == null)
        {
            node.HasGameObject = Instantiate(CurrentCharacter, new Vector3(node.Pos.x, interactPosition.y, node.Pos.z), Quaternion.Euler(0, CurrentAngle, 0));
            Grids.Instance.NodeArray[node.Numberx, node.Numbery].CharacterNumber = ObjectPoolManager.Instance.GetCharacter(CurrentCharacter);
            Grids.Instance.NodeArray[node.Numberx, node.Numbery].Rotation = CurrentAngle;
        }
    }

    /// <summary>
    /// 刪除NPC
    /// </summary>
    /// <param name="node">刪除node</param>
    void DeleteCharacter(Node node)
    {
        if (node.HasGameObject != null)
        {
            DestroyNodePoint(node);
        }
    }

    /// <summary>
    /// 移動NPC
    /// </summary>
    /// <param name="node">移動node</param>
    void MoveCharacter(Node node, Vector3 interactPosition)
    {
        if (CurrentNode.HasGameObject == null)
        {
            if (node.HasGameObject == null)
            {
                return;
            }
            else
            {
                Add(node);
            }
        }
        else
        {
            if (CurrentNode.HasGameObject == node.HasGameObject)
            {
                Debug.LogWarning("相同Object");
                return;
            }
            if (node.HasGameObject == null)
            {
                MoveTo(node, interactPosition);
            }
            else
            {
                Debug.LogWarning("目標點已有NPC");
            }
        }
    }

    /// <summary>
    /// 移動至目標Node點
    /// </summary>
    /// <param name="node">目標node</param>
    void MoveTo(Node node, Vector3 interactPosition)
    {
        MoveNodePoint(node, interactPosition);
        DestroyNodePoint(CurrentNode);
    }

    /// <summary>
    /// 移動Node資料
    /// </summary>
    /// <param name="node">目標Node</param>
    void MoveNodePoint(Node node, Vector3 interactPosition)
    {
        var movenode = Grids.Instance.NodeArray[node.Numberx, node.Numbery];
        movenode.HasGameObject = Instantiate(CurrentNode.HasGameObject, new Vector3(node.Pos.x, interactPosition.y, node.Pos.z), CurrentNode.HasGameObject.transform.rotation);
        movenode.HasGameObject.name = CurrentNode.HasGameObject.name;
        movenode.CharacterNumber = CurrentNode.CharacterNumber;
        movenode.Rotation = CurrentNode.Rotation;
        movenode.Sex = CurrentNode.Sex;
        movenode.SerialNumber = CurrentNode.SerialNumber;
        movenode.Content = CurrentNode.Content;
    }

    /// <summary>
    /// 刪除Node以及存檔訊息
    /// </summary>
    ///<param name="node">目標node</param>
    void DestroyNodePoint(Node node)
    {
        Destroy(Grids.Instance.NodeArray[node.Numberx, node.Numbery].HasGameObject);
        Grids.Instance.NodeArray[node.Numberx, node.Numbery].CharacterNumber = 0;
        Grids.Instance.NodeArray[node.Numberx, node.Numbery].Rotation = 0;
        Grids.Instance.NodeArray[node.Numberx, node.Numbery].Sex = 0;
        Grids.Instance.NodeArray[node.Numberx, node.Numbery].Content = "";
        Grids.Instance.NodeArray[node.Numberx, node.Numbery].SerialNumber = 0;
        GridDetectHelper.Instance.DetectGridMat.color = Color.white;
    }
}
