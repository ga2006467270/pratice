﻿using System.Collections.Generic;
using UnityEngine;

public class PlantNPCSaveUI : NPCSaveUIManager
{
    public override void Awake()
    {
        base.Awake();
        List<string> Option = new List<string>();
        foreach (GameObject character in ObjectPoolManager.Instance.CharacterObjectPool)
        {
            Option.Add(character.name);
        }
        mCharacterDropdown.AddOptions(Option);
    }
}
