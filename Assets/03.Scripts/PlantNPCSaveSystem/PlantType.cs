﻿using UnityEngine;
using UnityEngine.EventSystems;

interface IPlantType
{
    void Plant();
}

class PCPlant : IPlantType
{
    Camera mMainCam;
    LayerMask mGroundLayer;

    public PCPlant(LayerMask layer)
    {
        mMainCam = Camera.main; 
        mGroundLayer = layer;
    }

    public void Plant()
    {
        var ray = mMainCam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        Physics.Raycast(ray, out hit, Mathf.Infinity, mGroundLayer);
        GridDetectHelper.Instance.GridDected(hit.point);
        if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject())
        {
            INode node = Grids.Instance.NodeFromWorldPoint(hit.point);
            if (!node.Walkable)
            {
                Debug.LogWarning("此地不可走");
                return;
            }
            PlantManager.Instance.DoWithNode(Grids.Instance.NodeArray[node.Numberx, node.Numbery], hit.point);
        }
    }
}