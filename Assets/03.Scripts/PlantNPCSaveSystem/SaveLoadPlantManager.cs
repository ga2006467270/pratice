﻿using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;

public class SaveLoadPlantManager : ISaveLoadInterface
{
    public void Save(string path, string name)
    {
        var binarayformatter = new BinaryFormatter();
        using (var stream = new FileStream(path + name + SaveFileFormat.SaveFromat(eSaveFileFormat.txt), FileMode.Create))
        {
            var data = new PlayerData();
            binarayformatter.Serialize(stream, data);
        }
    }

    public void Load(string path)
    {
        var binarayformatter = new BinaryFormatter();
        using (var stream = new FileStream(path + SaveFileFormat.SaveFromat(eSaveFileFormat.txt), FileMode.Open))
        {
            var data = binarayformatter.Deserialize(stream) as PlayerData;
            SaveLoadPlant.Instance.CleanData();
            SaveLoadPlant.Instance.SetLoadData(data);
        }
    }
}

[Serializable]
public class PlayerData
{
    public int[,] SaveNumber;
    public float[,] SaveRotate;
    public string[,] NPCName;
    public float[,] YAxis;

    public PlayerData()
    {
        SaveNumber = new int[Grids.Instance.Bound, Grids.Instance.Bound];
        SaveRotate = new float[Grids.Instance.Bound, Grids.Instance.Bound];
        NPCName = new string[Grids.Instance.Bound, Grids.Instance.Bound];
        YAxis = new float[Grids.Instance.Bound, Grids.Instance.Bound];
        for (var boundx = 0; boundx < Grids.Instance.Bound; boundx++)
        {
            for (var boundy = 0; boundy < Grids.Instance.Bound; boundy++)
            {
                if (Grids.Instance.NodeArray[boundx, boundy].HasGameObject != null)
                {
                    SaveNumber[boundx, boundy] = Grids.Instance.NodeArray[boundx, boundy].CharacterNumber;
                    SaveRotate[boundx, boundy] = Grids.Instance.NodeArray[boundx, boundy].Rotation;
                    NPCName[boundx, boundy] = Grids.Instance.NodeArray[boundx, boundy].HasGameObject.name;
                    YAxis[boundx, boundy] = Grids.Instance.NodeArray[boundx, boundy].HasGameObject.transform.position.y;
                }
            }
        }
    }
}
