﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class NPCSaveUIManager : MonoBehaviour
{
    const int MAX_SET_ANGLE = 315;
    const int SET_ANGLE = 45;
    const int MAX_ANGLE = 360;
    const int MIN_ANGLE = 0;

    public static NPCSaveUIManager Instance
    {
        get
        {
            if (mInstance == null)
            {
                var gamemanager = new GameObject("PlantNPCSaveUI");
                gamemanager.AddComponent<NPCSaveUIManager>();
            }
            return mInstance;
        }
    }

    protected string mPath
    {
        set
        {
            mFilePath = value;
            if (mPath == "")
            {
                Debug.LogWarning("你取消了路徑");
            }
            return;
        }
        get
        {
            return mFilePath;
        }
    }

    protected string mFilePath;
    [SerializeField] protected Text mAngleValue;
    [SerializeField] protected Text mCurrentCharacterText;
    [SerializeField] protected InputField mCharacterNumberInputText;
    [SerializeField] protected Dropdown mCharacterDropdown;

    static NPCSaveUIManager mInstance;

    public virtual void Awake()
    {
        mInstance = this;
        AddCharacterDropDownOption();
    }

    /// <summary>
    /// 轉換至Add Mode
    /// </summary>
    public void ChangeToAdd()
    {
        PlantManager.Instance.EditorMode = PlantManager.eEditorMode.Add;
    }

    /// <summary>
    /// 轉換至Delete Mode
    /// </summary>
    public void ChangeToDelete()
    {
        PlantManager.Instance.EditorMode = PlantManager.eEditorMode.Delete;
    }

    /// <summary>
    /// 轉換至Move Mode
    /// </summary>
    public void ChangeToMove()
    {
        PlantManager.Instance.EditorMode = PlantManager.eEditorMode.Move;
    }

    /// <summary>
    /// 轉換至editor Mode
    /// </summary>
    public void ChangeToEditor()
    {
        PlantManager.Instance.EditorMode = PlantManager.eEditorMode.Edit;
    }

    /// <summary>
    /// 角色面對方向
    /// </summary>
    /// <param name="direction">方向 以正負號表示</param>
    public void AngleButton(int direction)
    {
        var direct = (direction < 0) ? -1 : 1;
        PlantManager.Instance.CurrentAngle += SET_ANGLE * direct;
        if (PlantManager.Instance.EditorMode == PlantManager.eEditorMode.Edit)
        {
            PlantManager.Instance.CurrentNode.Rotation = PlantManager.Instance.CurrentAngle;
        }
        if (PlantManager.Instance.CurrentAngle >= MAX_ANGLE)
        {
            PlantManager.Instance.CurrentAngle = MIN_ANGLE;
        }
        else if (PlantManager.Instance.CurrentAngle < MIN_ANGLE)
        {
            PlantManager.Instance.CurrentAngle = MAX_SET_ANGLE;
        }
        ChangeAngleButtonValue();
    }

    /// <summary>
    /// 更換目前角色編號
    /// </summary>
    public void ChangeCurrentCharacterNumber()
    {
        var inputtext = mCharacterNumberInputText.text;
        mCharacterNumberInputText.text = "";
        int characternumber;
        var isint = int.TryParse(inputtext, out characternumber);

        if (PlantManager.Instance.EditorMode == PlantManager.eEditorMode.Edit)
        {
            if (!isint)
            {
                IsCharacterString(inputtext);
                return;
            }
        }

        if (characternumber < ObjectPoolManager.Instance.CharacterObjectPool.Count && characternumber > 0)
        {
            PlantManager.Instance.CurrentCharacter = ObjectPoolManager.Instance.CharacterObjectPool[int.Parse(inputtext) - 1];
            PlantManager.Instance.CurrentNode.CharacterNumber = int.Parse(inputtext);
            mCurrentCharacterText.text = PlantManager.Instance.CurrentCharacter.name;
        }
        else
        {
            Debug.LogWarning("編號超出角色種類範圍");
            return;
        }

        if (PlantManager.Instance.EditorMode == PlantManager.eEditorMode.Edit)
        {
            ChangeCharacter(int.Parse(inputtext) - 1);
        }
    }

    /// <summary>
    /// 存檔
    /// </summary>
    public void SavePlantData()
    {
        SaveLoadPlant.Instance.Save(Application.dataPath, "/BinaryData/NpcData");
    }

    /// <summary>
    /// 下滑卷軸偵測
    /// </summary>
    /// <param name="index">選到第幾個數字</param>
    public void DropDownIndexChange(int index)
    {
        PlantManager.Instance.CurrentCharacter = ObjectPoolManager.Instance.CharacterObjectPool[index];
        PlantManager.Instance.CurrentNode.CharacterNumber = index + 1;
        if (PlantManager.Instance.EditorMode == PlantManager.eEditorMode.Edit)
        {
            ChangeCharacter(index);
        }
    }

    /// <summary>
    /// 讀檔
    /// </summary>
    public void LoadPlantData()
    {
        SaveLoadPlant.Instance.Load(Application.dataPath + "/BinaryData/NpcData");
        ResetUserSetting();
    }

    /// <summary>
    /// 清空所有Node資料
    /// </summary>
    public void ClearPlantData()
    {
        foreach (Node node in Grids.Instance.NodeArray)
        {
            Destroy(node.HasGameObject);
            node.HasGameObject = null;
            node.CharacterNumber = 0;
            node.Rotation = 0;
        }
        ResetUserSetting();
    }

    /// <summary>
    /// 讓UI與當前設定同步
    /// </summary>
    public virtual void SetEditorValue()
    {
        mAngleValue.text = "角度 :" + PlantManager.Instance.CurrentNode.HasGameObject.transform.eulerAngles.y;
        PlantManager.Instance.CurrentAngle = (int)PlantManager.Instance.CurrentNode.HasGameObject.transform.eulerAngles.y;
        mCurrentCharacterText.text = PlantManager.Instance.CurrentNode.HasGameObject.name;
    }

    /// <summary>
    /// 開場時將角色名加入DropDown
    /// </summary>
    void AddCharacterDropDownOption()
    {
        List<string> charactername = new List<string>();
        foreach (GameObject character in ObjectPoolManager.Instance.CharacterObjectPool)
        {
            charactername.Add(character.name);
        }
        mCharacterDropdown.AddOptions(charactername);
    }

    /// <summary>
    /// 更改角度說明
    /// </summary>
    void ChangeAngleButtonValue()
    {
        mAngleValue.text = "角度 :" + PlantManager.Instance.CurrentAngle;
    }

    /// <summary>
    /// 檢測是否為名子
    /// </summary>
    /// <param name="inputText">The inputtext.</param>
    void IsCharacterString(string inputText)
    {
        PlantManager.Instance.CurrentNode.HasGameObject.name = inputText;
        mCurrentCharacterText.text = PlantManager.Instance.CurrentNode.HasGameObject.name;
    }

    /// <summary>
    /// 更換角色
    /// </summary>
    /// <param name="characterNumber">角色清單number</param>
    void ChangeCharacter(int characterNumber)
    {
        float currenty = PlantManager.Instance.CurrentNode.HasGameObject.transform.position.y;
        var name = PlantManager.Instance.CurrentNode.HasGameObject.name;
        Destroy(PlantManager.Instance.CurrentNode.HasGameObject);
        PlantManager.Instance.CurrentNode.HasGameObject = Instantiate(ObjectPoolManager.Instance.CharacterObjectPool[characterNumber], new Vector3(PlantManager.Instance.CurrentNode.Pos.x, currenty, PlantManager.Instance.CurrentNode.Pos.z), Quaternion.Euler(0, PlantManager.Instance.CurrentNode.Rotation, 0));
        PlantManager.Instance.CurrentNode.HasGameObject.name = name;
    }

    /// <summary>
    /// 重設User目前的Enum Mode 暫定只有讀檔才會Reset
    /// </summary>
    void ResetUserSetting()
    {
        PlantManager.Instance.CurrentNode = new Node();
        PlantManager.Instance.EditorMode = PlantManager.eEditorMode.Add;
        PlantManager.Instance.CurrentAngle = 0;
        ChangeAngleButtonValue();
    }
}
