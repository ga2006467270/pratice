﻿using System.Collections.Generic;
using UnityEngine;

public class SaveLoadPlant : MonoBehaviour
{
    public static SaveLoadPlant Instance
    {
        get
        {
            if (mInstance == null)
            {
                var gamemanager = new GameObject("SaveLoadPlant");
                gamemanager.AddComponent<SaveLoadPlant>();
            }
            return mInstance;
        }
    }

    static SaveLoadPlant mInstance;

    public void Save(string path, string name)
    {
        SaveLoad save = new SaveLoad(SaveLoad.eSystemType.PlantNPC);
        save.Save(path, name);
    }

    public void Load(string path)
    {
        SaveLoad load = new SaveLoad(SaveLoad.eSystemType.PlantNPC);
        load.Load(path);
    }

    public void CleanData()
    {
        foreach (Node node in Grids.Instance.NodeArray)
        {
            Destroy(node.HasGameObject);
            node.HasGameObject = null;
            node.CharacterNumber = 0;
            node.Rotation = 0;
            node.Sex = 0;
            node.SerialNumber = 0;
            node.Content = "";
            node.CanBattle = false;
            node.CanTalk = false;
        }
    }

    public void SetLoadData(PlayerData data)
    {
        var bound = Grids.Instance.Bound;
        for (var boundx = 0; boundx < bound; boundx++)
        {
            for (var boundy = 0; boundy < bound; boundy++)
            {
                if (data.SaveNumber[boundx, boundy] != 0)
                {
                    Grids.Instance.NodeArray[boundx, boundy].CharacterNumber = data.SaveNumber[boundx, boundy];
                    Grids.Instance.NodeArray[boundx, boundy].Rotation = data.SaveRotate[boundx, boundy];
                    Grids.Instance.NodeArray[boundx, boundy].HasGameObject = Instantiate(ObjectPoolManager.Instance.GetCharacter(Grids.Instance.NodeArray[boundx, boundy].CharacterNumber), new Vector3(Grids.Instance.NodeArray[boundx, boundy].Pos.x, data.YAxis[boundx, boundy], Grids.Instance.NodeArray[boundx, boundy].Pos.z), Quaternion.Euler(new Vector3(0, Grids.Instance.NodeArray[boundx, boundy].Rotation, 0)));
                    Grids.Instance.NodeArray[boundx, boundy].HasGameObject.name = data.NPCName[boundx, boundy];
                }
            }
        }
    }

    public void SetNPCInteractLoadData(List<InteractNPCData.sMonsterData> datas, bool addInteractable)
    {
        var bound = Grids.Instance.Bound;
        foreach (InteractNPCData.sMonsterData data in datas)
        {
            Grids.Instance.NodeArray[data.Boundx, data.Boundy].Rotation = data.SaveRotate;
            Grids.Instance.NodeArray[data.Boundx, data.Boundy].SerialNumber = data.SerialNumber;
            Grids.Instance.NodeArray[data.Boundx, data.Boundy].CharacterNumber = data.NPCNumber;
            Grids.Instance.NodeArray[data.Boundx, data.Boundy].HasGameObject = Instantiate(ObjectPoolManager.Instance.GetCharacter(Grids.Instance.NodeArray[data.Boundx, data.Boundy].CharacterNumber), new Vector3(Grids.Instance.NodeArray[data.Boundx, data.Boundy].Pos.x, data.YAxis, Grids.Instance.NodeArray[data.Boundx, data.Boundy].Pos.z), Quaternion.Euler(new Vector3(0, Grids.Instance.NodeArray[data.Boundx, data.Boundy].Rotation, 0)));
            if (data.NPCName != "null")
            {
                Grids.Instance.NodeArray[data.Boundx, data.Boundy].HasGameObject.name = data.NPCName;
            }
            Grids.Instance.NodeArray[data.Boundx, data.Boundy].Sex = data.Sex;
            if (data.Content!= "null")
            {
                Grids.Instance.NodeArray[data.Boundx, data.Boundy].Content = data.Content;
            }
            if (ObjectPoolManager.Instance.SystemType == ObjectPoolManager.eSystemType.BattleSystem)
            {
                Grids.Instance.NodeArray[data.Boundx, data.Boundy].CanBattle = data.CanBattle;
                Grids.Instance.NodeArray[data.Boundx, data.Boundy].CharacterBattleNumber[0] = data.MonsterNumber[0];
                Grids.Instance.NodeArray[data.Boundx, data.Boundy].CharacterBattleNumber[1] = data.MonsterNumber[1];
                Grids.Instance.NodeArray[data.Boundx, data.Boundy].CharacterBattleNumber[2] = data.MonsterNumber[2];
                Grids.Instance.NodeArray[data.Boundx, data.Boundy].CharacterBattleNumber[3] = data.MonsterNumber[3];
                Grids.Instance.NodeArray[data.Boundx, data.Boundy].CharacterBattleNumber[4] = data.MonsterNumber[4];
                Grids.Instance.NodeArray[data.Boundx, data.Boundy].CharacterBattleNumber[5] = data.MonsterNumber[5];
            }
            Grids.Instance.NodeArray[data.Boundx, data.Boundy].CanTalk = data.CanTalk;
            if (addInteractable)
            {
                NPCInteractable npcinteract = Grids.Instance.NodeArray[data.Boundx, data.Boundy].HasGameObject.AddComponent<NPCInteractable>();
                npcinteract.SetBound(data.Boundx, data.Boundy);
            }
        }
    }

    public void CleanNPCInteractData()
    {
        foreach (Node node in Grids.Instance.NodeArray)
        {
            Destroy(node.HasGameObject);
            node.HasGameObject = null;
            node.CharacterNumber = 0;
            node.Rotation = 0;
            node.SerialNumber = 0;
            node.Sex = 0;
            node.Content = null;
        }
    }

    void Awake()
    {
        mInstance = this;
    }
}
