﻿using UnityEngine;

public class CameraController : MonoBehaviour
{
    public enum eSystemType
    {
        None,
        KeyBoard,
        Mouse,
        Follow,
    }

    public static CameraController Instance
    {
        get
        {
            if (mInstance == null)
            {
                var gamemanager = new GameObject("CameraController");
                gamemanager.AddComponent<CameraController>();
            }
            return mInstance;
        }
    }

    public eSystemType SystemType;

    /// <summary>
    /// 預設速度
    /// </summary>
    static CameraController mInstance;
    [SerializeField] float mCameraSpeed;
    ICameraType mCameraType;
    CameraStore mFactoryStore;

    public void ChangeCameraType(eSystemType cameraType)
    {
        mFactoryStore = new CameraStore(new SimpleCameraFactory());
        mCameraType = mFactoryStore.ObjectOrder(cameraType, mCameraSpeed);
    }

    public void SetCamera(Vector3 pos, Quaternion rotation)
    {
        gameObject.transform.position = pos;
        gameObject.transform.rotation = rotation;
    }

    void Awake()
    {
        mInstance = this;
    }

    private void Start()
    {
        ChangeCameraType(SystemType);
    }

    void Update()
    {
        mCameraType.CameraMove(gameObject, Camera.main.gameObject);
    }
}

public class CameraStore
{
    SimpleCameraFactory mFactory;

    public CameraStore(SimpleCameraFactory factory)
    {
        mFactory = factory;
    }

    public ICameraType ObjectOrder(CameraController.eSystemType cameraEnum, float cameraSpeed)
    {
        ICameraType cameratype;
        cameratype = mFactory.CreateObject(cameraEnum);
        cameratype.SetCameraSpeed(cameraSpeed);
        return cameratype;
    }
}

public class SimpleCameraFactory
{
    public ICameraType CreateObject(CameraController.eSystemType cameraType)
    {
        switch (cameraType)
        {
            case CameraController.eSystemType.KeyBoard:
                return new KeyBoardCamera();
            case CameraController.eSystemType.Mouse:
                return new MouseMoveCamera();
            case CameraController.eSystemType.Follow:
                return new FollowCamera();
            case CameraController.eSystemType.None:
                return new StaticCamera();
        }
        return null;
    }
}

