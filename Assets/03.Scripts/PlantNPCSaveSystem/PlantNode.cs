﻿using UnityEngine;

public class Node : INode, IHeapItem<Node>
{
    public GameObject HasGameObject
    {
        get
        {
            return mHasGameObject;
        }
        set
        {
            mHasGameObject = value;
            if (value != null)
            {
                mHasGameObject.name = value.name;
            }
        }
    }

    public float FCost
    {
        get
        {
            return GCost + HCost;
        }
    }

    public int HeapIndex
    {
        get
        {
            return mHeapIndex;
        }
        set
        {
            mHeapIndex = value;
        }
    }

    public float Rotation
    {
        get
        {
            return mRotation;
        }
        set
        {
            mRotation = value;
            if (HasGameObject != null)
            {
                HasGameObject.transform.rotation = Quaternion.Euler(0, Rotation, 0);
            }
        }
    }

    public bool Walkable
    {
        get
        {
            return mWalkable;
        }

        set
        {
            mWalkable = value;
        }
    }

    public Vector3 Pos
    {
        get
        {
            return mPos;
        }
        set
        {
            mPos = value;
        }
    }

    public int Numberx
    {
        get
        {
            return mNumberx;
        }
        set
        {
            mNumberx = value;
        }
    }

    public int Numbery
    {
        get
        {
            return mNumbery;
        }
        set
        {
            mNumbery = value;
        }
    }

    public float NumberHight
    {
        get
        {
            return mNumberHight;
        }
        set
        {
            mNumberHight = value;
        }
    }

    public int CharacterNumber
    {
        get
        {
            return mCharacterNumber;
        }
        set
        {
            mCharacterNumber = value;
        }
    }

    public string Content
    {
        get
        {
            return mContent;
        }
        set
        {
            mContent = value;
        }
    }

    public int SerialNumber
    {
        get
        {
            return mSerialNumber;
        }
        set
        {
            mSerialNumber = value;
        }
    }

    public int Sex
    {
        get
        {
            return mSex;
        }
        set
        {
            mSex = value;
        }
    }

    public int HCost
    {
        get
        {
            return mHCost;
        }
        set
        {
            mHCost = value;
        }
    }

    public int GCost
    {
        get
        {
            return mGCost;
        }

        set
        {
            mGCost = value;
        }
    }

    public Node ParentNode
    {
        get
        {
            return mParentNode;
        }

        set
        {
            mParentNode = value;
        }
    }

    public bool CanBattle
    {
        get
        {
            return mCanBattle;
        }

        set
        {
            mCanBattle = value;
        }
    }

    public int[] CharacterBattleNumber
    {
        get
        {
            return mCharacterBattleNumber;
        }

        set
        {
            mCharacterBattleNumber = value;
        }
    }

    public bool CanTalk
    {
        get
        {
            return mCanTalk;
        }

        set
        {
            mCanTalk = value;
        }
    }

    bool mCanBattle;
    bool mCanTalk;
    int[] mCharacterBattleNumber = new int[BattleManager.MAX_CHARACTER];
    int mCharacterNumber;
    string mContent;
    int mSerialNumber;
    int mSex;
    int mHCost;
    int mGCost;
    Node mParentNode;
    GameObject mHasGameObject;
    float mRotation;
    bool mWalkable;
    int mHeapIndex;
    int mNumberx;
    int mNumbery;
    float mNumberHight;
    Vector3 mPos;

    public void InitializeNode(Vector3 pos, bool walkable, int numberx, int numbery, float numberHight)
    {
        Walkable = walkable;
        Pos = pos;
        Numberx = numberx;
        Numbery = numbery;
        NumberHight = numberHight;
    }

    public int CompareTo(Node nodeToCompare)
    {
        int compare = FCost.CompareTo(nodeToCompare.FCost);
        if (compare == 0)
        {
            compare = HCost.CompareTo(nodeToCompare.HCost);
        }
        return -compare;
    }
}
