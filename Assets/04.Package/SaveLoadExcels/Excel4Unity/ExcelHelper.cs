﻿using OfficeOpenXml;
using System.IO;

public class ExcelHelper
{
    public static Excel LoadExcel(string path)
    {
        FileInfo file = new FileInfo(path);
        ExcelPackage excelpackage = new ExcelPackage(file);
        Excel xls = new Excel(excelpackage.Workbook);
        return xls;
    }

	public static Excel CreateExcel(string path)
    {
		ExcelPackage excelpackage = new ExcelPackage ();
		excelpackage.Workbook.Worksheets.Add ("sheet");
		Excel xls = new Excel(excelpackage.Workbook);
		SaveExcel (xls, path);
		return xls;
	}

    public static void SaveExcel(Excel xls, string path)
    {
        FileInfo output = new FileInfo(path);
        ExcelPackage excelpackage = new ExcelPackage();
        for (int i = 0; i < xls.Tables.Count; i++)
        {
            ExcelTable table = xls.Tables[i];
            ExcelWorksheet sheet = excelpackage.Workbook.Worksheets.Add(table.TableName);
            for (int row = 1; row <= table.NumberOfRows; row++)
            {
                for (int column = 1; column <= table.NumberOfColumns; column++)
                {
                    sheet.Cells[row, column].Value = table.GetValue(row, column);
                }
            }
        }
        excelpackage.SaveAs(output);
    }
}
