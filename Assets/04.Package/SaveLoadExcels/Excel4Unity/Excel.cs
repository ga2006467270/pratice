﻿using System.Collections.Generic;
using OfficeOpenXml;

public class Excel
{
    const int TABLE = 0;

    public List <ExcelTable> Tables = new List<ExcelTable>();

    public Excel()
    {
		
    }

    public Excel(ExcelWorkbook worBool)
    {
        for (int i = 1; i <= worBool.Worksheets.Count; i++)
        {
            ExcelWorksheet sheet = worBool.Worksheets[i];
            ExcelTable table = new ExcelTable(sheet);
            Tables.Add(table);
        }
    }

    public void ShowLog()
    {
        for (int i = 0; i < Tables.Count; i++)
        {
            Tables[i].ShowLog();
        }
    }

    public string[,,] GetLogArray()
    {
        string[,,] logarray = new string[Tables.Count, Tables[TABLE].NumberOfRows, Tables[TABLE].NumberOfColumns];
        for (int table = 0; table < Tables.Count; table++)
        {
            for (int row = 0; row < Tables[table].NumberOfRows; row++)
            {
                for (int column = 0; column < Tables[table].NumberOfColumns; column++)
                {
                    logarray[table, row, column] = Tables[table].SingleLog(row,column);
                }
            }
        }
        return logarray;
    }

    public void AddTable(string name)
    {
        ExcelTable table = new ExcelTable();
        table.TableName = name;
        Tables.Add(table);
    }
}
