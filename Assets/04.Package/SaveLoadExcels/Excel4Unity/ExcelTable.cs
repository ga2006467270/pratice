﻿using UnityEngine;
using System.Collections.Generic;
using OfficeOpenXml;

public class ExcelTable
{
    public int LogRows
    {
        get
        {
            return NumberOfRows;
        }
    }

    public int LogColums
    {
        get
        {
            return NumberOfColumns;
        }
    }

    public string TableName;
    public int NumberOfRows;
    public int NumberOfColumns;
    public Vector2 Position;

    Dictionary <int, Dictionary<int, ExcelTableCell>> mCells = new Dictionary<int, Dictionary<int, ExcelTableCell>>();

    public ExcelTable()
    {
    }

    public ExcelTable(ExcelWorksheet sheet)
    {
        TableName = sheet.Name;
	if (sheet.Dimension != null)
	{
		NumberOfRows = sheet.Dimension.Rows;
		NumberOfColumns = sheet.Dimension.Columns;
	}
	else
	{
		NumberOfRows = 0;
		NumberOfColumns = 0;
	}
        for (int row = 1; row <= NumberOfRows; row++)
        {
            for (int column = 1; column <= NumberOfColumns; column++)
            {
                string value = ""; //default value for empty cell
	            if (sheet.Cells [row, column].Value != null)
	            {
		            value = sheet.Cells [row, column].Value.ToString ();
	            }
                SetValue(row, column, value);
            }
        }
    }

    public ExcelTableCell SetValue(int row, int column, string value)
    {
		CorrectSize(row, column);
        if (!mCells.ContainsKey(row))
        {
            mCells[row] = new Dictionary<int, ExcelTableCell>();
        }
        if (mCells[row].ContainsKey(column))
        {
            mCells[row][column].Value = value;

            return mCells[row][column];
        }
        else
        {
            ExcelTableCell cell = new ExcelTableCell(row, column, value);
            mCells[row][column] = cell;
            return cell;
        }
    }

    public object GetValue(int row, int column)
    {
        ExcelTableCell cell = GetCell(row, column);
        if (cell != null)
        {
            return cell.Value;
        }
        else
        {
            return SetValue(row, column,"").Value;
        }
    }

    public ExcelTableCell GetCell(int row, int column)
    {
        if (mCells.ContainsKey(row))
        {
            if (mCells[row].ContainsKey(column))
            {
                return mCells[row][column];
            }
        }
        return null;
    }

    public void CorrectSize(int row, int column)
    {
        NumberOfRows = Mathf.Max(row, NumberOfRows);
        NumberOfColumns = Mathf.Max(column, NumberOfColumns);
    }

    public void SetCellTypeRow(int rowIndex, eExcelTableCellType type)
    {
        for (int column = 1; column <= NumberOfColumns; column++)
        {
            ExcelTableCell cell = GetCell(rowIndex, column);
            if (cell != null)
            {
                cell.Type = type;
            }
        }
    }

    public void SetCellTypeColumn(int columnIndex, eExcelTableCellType type, List<string> values = null)
    {
        for (int row = 1; row <= NumberOfRows; row++)
        {
            ExcelTableCell cell = GetCell(row, columnIndex);
            if (cell != null)
            {
                cell.Type = type;
                if (values != null)
                {
                    cell.ValueSelected = values;
                }
            }
        }
    }

    public void ShowLog()
    {
        string msg = "";
        for (int row = 1; row <= NumberOfRows; row++)
        {
            for (int column = 1; column <= NumberOfColumns; column++)
            {
                msg += string.Format("{0} ", GetValue(row, column));
            }
            msg += "\n";
        }

    }

    public string SingleLog(int logListRow,int logListcolumn)
    {
        string logstring;
        logstring = string.Format("{0}", GetValue(logListRow+1, logListcolumn+1));
        return logstring;
    }
}
