  Shader "vertexPainter/vB_Diffuse_3tex" {
    Properties {
        _Color              ("Main Color", Color) = (1,1,1,1)
        _MainTex1           ("Texture 1 (RGB)", 2D) = "white" {}
        _MainTex2           ("Texture 2 (RGB)", 2D) = "white" {}
        _MainTex3           ("Texture 3 (RGB)", 2D) = "white" {}
        _BumpMap1           ("BumpMap 1 (RGB)", 2D) = "bump" {}
        _BumpMap2           ("BumpMap 2 (RGB)", 2D) = "bump" {}
        _BumpMap3           ("BumpMap 3 (RGB)", 2D) = "bump" {}
    }

    SubShader {
        
      Tags { "RenderType" = "Opaque" }
      CGPROGRAM
      #pragma surface surf Lambert vertex:vert
      #pragma target 3.0
      
        struct Input {
            float2 uv_MainTex1      : TEXCOORD0;
            float2 uv_MainTex2      : TEXCOORD1;
            float2 uv_MainTex3      : TEXCOORD2;
            
            float4 _color           : TEXCOORD3;
        };
        
        void vert (inout appdata_full v, out Input o) {
            UNITY_INITIALIZE_OUTPUT(Input, o);
            o._color = (v.color);
        }

        fixed4 _Color;
        sampler2D _MainTex1, _MainTex2, _MainTex3;
        sampler2D _BumpMap1, _BumpMap2, _BumpMap3;
      
        void surf (Input IN, inout SurfaceOutput o) {
            fixed4 col =
            tex2D( _MainTex1, IN.uv_MainTex1)*IN._color.r +
            tex2D( _MainTex2, IN.uv_MainTex2)*IN._color.g +
            tex2D( _MainTex3, IN.uv_MainTex3)*IN._color.b;
            
            fixed3 normal =
            UnpackNormal(tex2D(_BumpMap1, IN.uv_MainTex1))*IN._color.r +
            UnpackNormal(tex2D(_BumpMap2, IN.uv_MainTex2))*IN._color.g +
            UnpackNormal(tex2D(_BumpMap3, IN.uv_MainTex3))*IN._color.b;
            
            o.Albedo = col * _Color.rgb;
            o.Normal = normal;
        }
      
      ENDCG

    }
FallBack "Diffuse"
}